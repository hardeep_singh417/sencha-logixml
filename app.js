/* 
 * created : 12 March 2014
 * Author  : Hardeep Singh
 * Project : Logixml
 */

/**
 * set path for multilingual
 */
Ext.Loader.setConfig({
    enabled: true,
    disableCaching:true,
    paths: {
        'Ext.ux': 'resources/ux'
    }
});

/**---------- Application Setup ------------------- */

Ext.application({     
        name : "logixml",        
        requires: [
            'Ext.ux.Localization', // Load multilingual js file and create object,
        ],     
       viewport: { 
            fullscreen : true,
            autoMaximize: true,
            masked: {
                xtype: 'loadmask',
                message: 'Loading...'
            },
            style : {
                backgroundColor : "#fff",
                width : "100%",
                height: "100%"
            },
            layout : { 
                type: 'fit',
            },
        },                    
        launch : function () 
        {            
            /**
             * call for creating components
             */
            defineComponents();
            
            /**
             * create model and store components  
             */
            load_model_store();
            
            /**
             * after creating model, stroe and others components, we can proceed next 
             */
            get_user();
            
            var rh, rw;
            
            rw = window.innerWidth/1024;
            rh = window.innerHeight/768;

            scale;
            if(rw != rh)
            {
                scale = rw - (rw - rh);   
            }
            else
            {
                scale = rw;
            }
            
            scale = (scale * 100) + 1;
            
            defaultQueryString += "&fontSize=" + scale;
            
            $("body").css("font-size", (scale) + "%");
            $("body > div").css("width", window.innerWidth);
            $("body > div").css("height", window.innerHeight);
            
            
            /**
             * after loading model
             */
            set_help_view_object();
            
            Ext.Viewport.add(
            {
                xtype : "bodyView",
                items : [
                    get_login_view_object()
                ],
                listeners : {                
                    activeitemchange : function (mainView, card, oldCard) {
                        //reset the global variables
                        last_taskbar_btn = "";
                    },
                }
            });
        }  
});
