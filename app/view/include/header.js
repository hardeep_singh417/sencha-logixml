/* 
 * created : 12 March 2014
 * author  : Hardeep Singh
 * project : Logixml
 */

/**
 * main header of application
 * @var header
 */

function get_main_header_object()
{
    return  {                
        docked: "top",
        xtype : "iframecomponent",
        id    : "header",
        url   : domain + server_path + "rdPage.aspx?rdReport=01_Header&lbxCategory=" + user.language + "&" + defaultQueryString,
        style : {        
            width: "100%",
            height: "5.55em",
            paddingRight : "10px",
            backgroundColor: "#fff",
            paddingTop : "0.7em" 
       }
    };
}