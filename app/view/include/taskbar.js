/* 
 * created : 12 March 2014
 * author  : Hardeep Singh
 * project : Logixml
 */

/**
 * taskbar click reqiure to prevent user to click on taskbar quickly
 * last_taskbar_btn store id of last click btn 
 */

var last_taskbar_btn;


/**
 * Comman Listner function for taskbar buttons
 * @param object btn
 * @param obhect event
 * @returns 
 */
function taskbar_listener_call(btn, event)
{  
    var current = mainView.getActiveItem(),
        innerItems = mainView.getInnerItems(),
        totalItems = innerItems.length,
        currentIndex = innerItems.indexOf(current)
    ;
    /**
     * This following for hideing the filter form.
     */
    var cmp =  $("#filter_form");
    if(typeof cmp != "undefined" )
    {
         $("#filter_form").css("-webkit-animation" , "hide 1s"); 
         $("#filter_form").css("-webkit-animation-fill-mode" , "forwards");
    }


    switch(btn._itemId)
    {
        case "sign_out":
            bodyView.animateActiveItem(0, 
            {
                type : 'slide',
                direction : "right"
            }); 

            mainView.animateActiveItem(0, 
            {
                type : 'slide',
                direction : "right"
            }); 
        break;

        case "home":
           mainView.animateActiveItem(0, 
            {
                type : 'slide',
                direction : "right"
            }); 
        break;

        case "help":
            var index = -1;
            for(var i in innerItems)
            {
                if(innerItems[i].id == "helpView")
                    index = parseInt(i);
            }

            if (index >=0)
                mainView.animateActiveItem(index, 
                {
                    type : 'slide',
                    direction : "left"
                });
            else
            {
               mainView.animateActiveItem(helpView, 
               {
                   type : 'slide',
                   direction : "left"
               });
            }
        break;

        case "setting":
            var index = -1;
            for(var i in innerItems)
            {
                if(innerItems[i].id == "settingView")
                    index = parseInt(i);
            }


            if (index >=0)
            {
                mainView.removeAt(index);                      
            }

            Ext.Function.defer(function()
            { 
                mainView.animateActiveItem(get_setting_view_object(), 
                {
                    type : 'slide',
                    direction : "left"
                });
            }, 100);              
        break;
    }
     
}

/**
 * function for single button on footer
 * @param String id
 * @param String img + path
 * @returns var
 */

function get_taskbar_btn (id, img, opacity)
{
    return {
        xtype: "button",                                                   
        itemId   : id,
         style: {
            backgroundColor: "transparent",
            opacity: opacity,
            width : "7.1875em",
            height : "4.125em",
            border: "none",
            borderRadius : "0",
            backgroundImage: "url('" + img+ "')",
            backgroundRepeat: "no-repeat",
            backgroundSize: "2.8125em 2.5em",
            backgroundPosition: "center"             
        },
        handler: function (btn, event){
            
            if (last_taskbar_btn != btn._itemId)
            {
                active_report = null;
                var op = btn._style.opacity;            
                btn.setStyle({ opacity: "0.75" });

                Ext.Function.defer(function()
                {                            
                   btn.setStyle({ opacity : op });                
                   taskbar_listener_call(btn, event);               
                },100);            
                
                last_taskbar_btn = btn._itemId;
            }
        }
    };
}


function taskbar(id, items)
{
    return {
      itemId : id,
      xtype: 'container',    
      docked : "bottom",
      layout:{
            type : "hbox",   pack : "center",    align : "middle"
      },
      style: {
           padding: "0.5em 0",      
      },
      items : items
    };
}


function getTaskBar(current_item_id)
{
    return taskbar("taskBar", [
    {
        xtype : "container",
        layout:{
                type : "hbox", pack : "start", align : "middle"
        },
        items : [            
            get_taskbar_btn("home", "img/home.png", current_item_id == "home" ? 0.5 : 1),    
            get_taskbar_btn("help", "img/help.png", current_item_id == "help" ? 0.5 : 1)
        ]
    },
    {
        xtype : "spacer"
    },
    {
        xtype : "container",
        layout:{
                type : "hbox", pack : "end", align : "middle"
        },
        items : [
            get_taskbar_btn("setting", "img/setting.png", current_item_id == "setting" ? 0.5 : 1),
            get_taskbar_btn("sign_out", "img/sign-out.png",  current_item_id == "sign_out" ? 0.5 : 1)
        ]
    }]);
}