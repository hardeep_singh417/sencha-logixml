/* 
 * static array and variable define 
 * created 18 Apr 2014
 */

var chart_report = new Array();
/**
 * ----------------------- Spend Report Array ----------------------------------
 */
chart_report['spend_month'] = {   
    category : "spend",
    icon  : "img/icon/month_blue.png",
    iframe: [
        { 
            id   : "spend_month_bar", 
            url  : "rdPage.aspx?rdReport=25_SpendSummaryByMonth"
        },
        { 
            id : "spend_month_line", 
            url : "rdPage.aspx?rdReport=25_SpendSummaryByMonth&chart-type=Line", 
        }    
    ]
};

chart_report['spend_country'] = {
    category : "spend",
    icon  : "img/icon/country_blue.png",
    iframe: [
        { 
            id : "spend_country_bar", 
            url : "rdPage.aspx?rdReport=25_SpendSummaryByCountry"
        },
        { 
            id : "spend_country_line", 
            url : "rdPage.aspx?rdReport=25_SpendSummaryByCountry&chart-type=Line", 
        }    
    ]
};

chart_report['spend_charge_type'] = {
    category : "spend",
    icon  : "img/icon/charge_type.png",
    iframe: [
        { 
            id : "spend_charge_type_bar", 
            url : "rdPage.aspx?rdReport=25_SpendSummaryByChargeType"
        },
        { 
            id : "spend_charge_type_line",
            url : "rdPage.aspx?rdReport=25_SpendSummaryByChargeType&chart-type=Line", 
        }    
    ]
};

chart_report['spend_cost_center'] = {
    category : "spend",
    icon  : "img/icon/cost_center.png",
    iframe: [
        { 
            id : "spend_cost_center_bar",
            url : "rdPage.aspx?rdReport=51_Cost_Center_Spend_Summary_by_Month"
        },
        { 
            id : "spend_cost_center_line", 
            url : "rdPage.aspx?rdReport=51_Cost_Center_Spend_Summary_by_Month&chart-type=Line", 
        }    
    ]
};

chart_report['spend_top_spender'] = {
    category : "spend",
    icon  : "img/icon/top_spender_blue.png",
    iframe: [
        { 
            id : "spend_top_spender_bar", 
            url : "rdPage.aspx?rdReport=27_HighSpendingUsers"
        },
        { 
            id : "spend_top_spender_line",
            url : "rdPage.aspx?rdReport=27_HighSpendingUsers&chart-type=Line", 
        }    
    ]
};

chart_report['spend_top_international'] = {
    category : "spend",
    icon  : "img/icon/top_international_blue.png",
    iframe: [
        { 
            id : "spend_top_international_bar",
            url : "rdPage.aspx?rdReport=25_InternationalTrafficDestinations"
        },
        { 
            id : "spend_top_international_line", 
            url : "rdPage.aspx?rdReport=25_InternationalTrafficDestinations&chart-type=Line", 
        }    
    ]
};

chart_report['spend_top_roaming'] = {
    category : "spend",
    icon  : "img/icon/top_roaming_blue.png",
    iframe: [
        { 
            id : "spend_top_roaming_bar",
            url : "rdPage.aspx?rdReport=26_RoamingTrafficDestinations"
        },
        { 
            id : "spend_top_roaming_line",
            url : "rdPage.aspx?rdReport=26_RoamingTrafficDestinations&chart-type=Line", 
        }    
    ]
};

/**
 * ----------------------- Usage Report Array ----------------------------------
 */

chart_report['usage_month'] = {
    category : "usage",
    icon  : "img/icon/month_green.png",
    iframe: [
        { 
            id : "usage_month_1",
            iframe : [
                {
                    id : "usage_month_1_1",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByMonth&organized-type=Calls"
                },
                { 
                    id : "usage_month_1_2", 
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByMonth&organized-type=Calls&chart-type=Line", 
                },
            ]
        },
        { 
            id  : "usage_month_3",           
            iframe : [
                {
                    id : "usage_month_3_1",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByMonth&organized-type=Data", 
                },
                {
                    id : "usage_month_3_2",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByMonth&organized-type=Data&chart-type=Line", 
                }
            ]
        },
        { 
            id  : "usage_month_2",                        
            iframe : [
                {
                    id : "usage_month_2_1",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByMonth&organized-type=Messaging",
                },
                {
                    id : "usage_month_2_2",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByMonth&organized-type=Messaging&chart-type=Line",
                }
            ]
        }        
    ]
};

chart_report['usage_country'] = {
    category : "usage",
    icon  : "img/icon/country_green.png",
    iframe: [
        { 
            id  : "usage_summary_1",           
            iframe : [
                {
                    id : "usage_summary_1_1",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByCountry&organized-type=Calls"
                },
                {
                    id : "usage_summary_1_2",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByCountry&organized-type=Calls&chart-type=Line", 
                }
            ]
        },
        {
            id  : "usage_summary_2",           
            iframe : [
                {
                    id : "usage_summary_2_1",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByCountry&organized-type=Data", 
                },
                {
                    id : "usage_summary_2_2",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByCountry&organized-type=Data&chart-type=Line", 
                }
            ]
        },
        {
            id  : "usage_summary_3",           
            iframe : [
                {
                    id : "usage_summary_3_1",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByCountry&organized-type=Messaging", 
                },
                {
                    id : "usage_summary_3_2",
                    url : "rdPage.aspx?rdReport=25_UsageSummaryByCountry&organized-type=Messaging&chart-type=Line", 
                }
            ]
        },
    ]
};

chart_report['usage_top_user'] = {
    category : "usage",
    icon  : "img/icon/top_spender_green.png",
    iframe: [
        { 
            id : "usage_top_spender_1",
            url : "rdPage.aspx?rdReport=25_TopHighVolumeUsers"
        },
        { 
            id : "usage_top_spender_2",
            url : "rdPage.aspx?rdReport=25_TopHighVolumeUsers&chart-type=Line", 
        }
    ]
};

chart_report['usage_top_international'] = {
    category : "usage",
    icon  : "img/icon/top_international_green.png",
    iframe: [
        {
            id  : "usage_top_international_1",           
            iframe : [
                {
                    id : "usage_top_international_1_1",
                    url : "rdPage.aspx?rdReport=25_InternationalTrafficDestinations_Usage&traffic-type=Calls"
                },
                {
                    id : "usage_top_international_1_2",
                    url : "rdPage.aspx?rdReport=25_InternationalTrafficDestinations_Usage&traffic-type=Calls&chart-type=Line", 
                }
            ]
        },
        {
            id  : "usage_top_international_2",           
            iframe : [
                {
                    id : "usage_top_international_2_1",
                    url : "rdPage.aspx?rdReport=25_InternationalTrafficDestinations_Usage&traffic-type=Data", 
                },
                {
                    id : "usage_top_international_2_2",
                    url : "rdPage.aspx?rdReport=25_InternationalTrafficDestinations_Usage&traffic-type=Data&chart-type=Line", 
                }
            ]
        }
        
    ]
};

chart_report['usage_top_roaming'] = {
    category : "usage",
    icon  : "img/icon/top_roaming_green.png",
    iframe: [
        {
            id  : "usage_top_roaming_1",           
            iframe : [
                {
                    id : "usage_top_roaming_1_1",
                    url : "rdPage.aspx?rdReport=25_RoamingTrafficDestinations_Usage&organized-type=Voice"
                },
                {
                    id : "usage_top_roaming_1_2",
                    url : "rdPage.aspx?rdReport=25_RoamingTrafficDestinations_Usage&organized-type=Voice&chart-type=Line", 
                }
            ]
        },
        {
            id  : "usage_top_roaming_2",           
            iframe : [
                {
                    id : "usage_top_roaming_2_1",
                    url : "rdPage.aspx?rdReport=25_RoamingTrafficDestinations_Usage&organized-type=Message", 
                },
                {
                    id : "usage_top_roaming_2_2",
                    url : "rdPage.aspx?rdReport=25_RoamingTrafficDestinations_Usage&organized-type=Message&chart-type=Line", 
                }
            ]
        },
        {
            id  : "usage_top_roaming_3",           
            iframe : [
                {
                    id : "usage_top_roaming_3_1",
                    url : "rdPage.aspx?rdReport=25_RoamingTrafficDestinations_Usage&organized-type=KB", 
                },
                {
                    id : "usage_top_roaming_3_2",
                    url : "rdPage.aspx?rdReport=25_RoamingTrafficDestinations_Usage&organized-type=KB&chart-type=Line", 
                }
            ]
        },
    ]
};

chart_report['usage_long_calls'] = {
    category : "usage",
    icon  : "img/icon/long_calls.png",
    iframe: [
        { 
            id : "long_calls_1",
            url : "rdPage.aspx?rdReport=25_TopLongDurationCalls"
        },
        { 
            id : "long_calls_2",
            url : "rdPage.aspx?rdReport=25_TopLongDurationCalls&chart-type=Line", 
        }    
    ]
};

/**
 * ----------------- Inventory ------------------------------------------
 */
chart_report['inventory_report'] = {
    category : "inventory",
    icon  : "img/icon/inventory.png",
    iframe: [
        { 
            id : "inventory_report_chart", 
            url : "rdPage.aspx?rdReport=25_InventorySummary"
        }
    ]
};
/**
 * ----------------- Admin ------------------------------------------
 */
chart_report['organization_hirechy'] = {
    category : "admin",
    icon  : "img/icon/organization.png",
    iframe: [
        { 
            id : "organization_hirechy_1", 
            url : "rdPage.aspx?rdReport=107_Admin_Organization_Hierarchy&chart-type=Tree"
        },
        { 
            id : "organization_hirechy_2", 
            url : "rdPage.aspx?rdReport=107_Admin_Organization_Hierarchy&chart-type=Table",
        }
    ]
};

chart_report['billfeed_status'] = { 
    category : "admin",
    icon  : "img/icon/billfeed_status.png",
    iframe: [
        { 
            id : "billfeed_status_chart", 
            url : "rdPage.aspx?rdReport=111_Admin_Invoice_Upload_Status"
        }
    ]
};

chart_report['exchange_rates'] = {
    category : "admin",
    icon  : "img/icon/exchange_rates.png",
    iframe: [
        { 
            id : "exchange_rates_1", 
            url : "rdPage.aspx?rdReport=00_Exchange_Rate"
        }        
    ]
};

