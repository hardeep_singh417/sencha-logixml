/* 
 * created : 12 March 2014
 * author  : Hardeep Singh
 * project : Logixml
 */
var mainView;

function get_main_view_img()
{
    /*if(typeof bmo_arr[user.bmo] != "undefined")
        return "img/1024x768/landscape/" + bmo_arr[user.bmo];
    else
    */
    return "img/1024x768/landscape/default.png";
}

function create_main_view(items)
{
    return {        
        xtype : "mainView",
        items : [
            get_main_header_object(),
            {
                xtype : "container",
                id    : "mainView",               
                layout : { 
                    type: 'card',
                    animation : { type : "slide", duration : 500}
                },
                
                items : [items],
                listeners : {                
                    activeitemchange : function (mainView, card, oldCard) {                
                       
                    },
                    painted : function () {
                        mainView = this;
                    },
                    activate : function () {
                        Ext.Viewport.setMasked(false);
                    }
                }
            }
        ]
    };
            
}
