/* 
 * created : 12 March 2014
 * author  : Hardeep Singh
 * project : Logixml
 */
 
var active_report, active_chart = null , chart_arrow = [];
var horizontal_chart_info = [], chart_count, chart_items = [];
var active_chart_index = 0, active_sub_chart_index = [], hcarousel, filter_icon, default_report_query_string, filter_form, report_color;
var filter_form_top, filter_form_height, have_sub_charts = false, default_report_filter;

/**
 * function called when ever chart to refresh  
 */
function refresh_chart(iframe, get_url_str)
{
    function load_iframe()
    {
        var frame = Ext.getCmp(iframe.id);
        
        /**
         * prevent to load url before compnent creation
         */
        if (frame && default_report_query_string)
        {
            var url = domain + server_path + iframe.url + "&" + default_report_query_string;
            
            if (get_url_str)
            {
                url += "&" + get_url_str.trim() ;
            }

            var temp = url + "#" + document.location.href;                        
            temp = temp.trim();
            var iframe_url = decodeURIComponent(frame.iframe.dom.src.trim()).trim();
            
            if (iframe_url != temp)
            {
                frame.iframe.dom.src = url + "#" + encodeURIComponent(document.location.href);
                Ext.Viewport.setMasked(true);
            }
        }
    }
     
    if (check_connection)
    {
        check_internet_connection({
            successFn : function ()
            {
                load_iframe();
            },
            failureFn : function ()
            {
				var cmp = Ext.getCmp(iframe.id);
                console.log(cmp);
                if (cmp)
                {
                    cmp = cmp.getParent();
                    var items = cmp.getItems();
                    var id = cmp.getId();

                    var cssClass = "";
                    switch(chart_report[active_report]['category'])
                    {
                        case "spend":
                            cssClass = "reload-cmp-content border-spend";
                        break;
                        case "usage":
                            cssClass = "reload-cmp-content border-usage";
                        break;
                        case "inventory":
                            cssClass = "reload-cmp-content border-inventory";
                        break;
                        case "admin":
                            if(active_report == "organization_hirechy")
                            {
                                cssClass = "reload-cmp-content border-organization";
                            }
                            else if(active_report == "billfeed_status")
                            {
                                cssClass = "reload-cmp-content border-billfeed";
                            }
                            else
                            {
                                cssClass = "reload-cmp-content border-exchange";
                            }
                        break ;
                    }
                    cmp.removeAll();
                                 
                    
                    cmp.add([
                        connection_failed_template({ h : "100%", w : "100%", cssClass : cssClass})
                    ]);
                }

            }
        });
    }
    else
    {
        load_iframe();
    }
        
}

function refresh_current_iframe()
{
    if( typeof hcarousel != "undefined" )
    {
        hcarousel.setActiveItem(active_chart_index);
        hcarousel.fireEvent('activeitemchange', hcarousel , hcarousel.getActiveItem());
    }
    else if (active_report)
    {
        refresh_all_charts();
    }
}

function refresh_all_charts()
{
    var get_url_str = convert_form_value_to_get_url();
    
    for(var i in chart_report[active_report]['iframe'])
    {     
        var iframe = chart_report[active_report]['iframe'][i];        
        /**
         * if there is subchart then following condition check required.
         */
        if(typeof iframe['iframe'] != "undefined")
        {
            for( var a in iframe['iframe'])
            {
                refresh_chart(iframe['iframe'][a], get_url_str);                                        
            }
        }
        else
        {
            refresh_chart(iframe, get_url_str);                                    
        }
    }
}

/**
 * function for createng filter component for each Report.
 * @param multidimensional array filter_data
 */
function set_filter_component (filter_data)
{
    if(!Ext.getCmp("filter_form") && filter_data.length > 0)
    { 
        console.log(filter_data);
        var items = [];    
        
        items.push ({
            xtype: 'container',
            style : {
               margin : "0px",
               width  : "100%",
            },
            items : [{
                xtype  : "container", 
                layout : "hbox",
                style  : {
                    padding : "0.3125em 0.625em"
                },
                items : [
                    {     
                        style :{
                            height : "2.5em",
                            width : "6.1875em",
                            backgroundImage : "url('"+ filter_icon+ "')",
                            backgroundPosition : "center",
                            backgroundSize : "100% auto",
                            backgroundRepeat : "no-repeat",
                        }
                    },
                    {
                        xtype : "spacer"
                    },
                    {
                        xtype : "button",
                        cls   : "button",
                        style: {                                                                                                 
                            height  : "2.1875em",
                            width   : "2.1875em",
                            margin  : "0.25em 0.625em 0 0",
                            backgroundColor : "transparent",
                            backgroundImage: "url('img/FilterBtn.png')",
                            backgroundRepeat: "no-repeat",
                            backgroundSize: "100% 100%",
                            border : "none"                            
                        },
                        handler: function (btn, event)
                        {        
                            btn.setStyle({
                                opacity : "0.7"
                            });
                                                                           
                            $("#filter_form").css("-webkit-animation" , "hide 1s"); /* Chrome, Safari, Opera */
                            $("#filter_form").css("-webkit-animation-fill-mode" , "forwards");
                            
                            refresh_current_iframe();
                            
                            Ext.Function.defer(function()
                            {            
                                btn.setStyle({
                                    opacity : "1"
                                });                                                       
                            }, 400);
                            
                        }
                    }
                ]
            }]
        });
        
        
        var reg_replace_space = new RegExp(' ', 'g');
        var label = '';
        
        for(var i in filter_data)
        {   
            switch(filter_data[i]['type'])
            {
                case "radio":                
                var child_items = [];
                    for( var a in filter_data[i]['options'])
                    {   
                       label = filter_data[i]['options'][a]['caption'].trim().replace(reg_replace_space, '_').toLowerCase();
                        
                       child_items.push({
                           xtype      : "radiofield",
                           labelCls   : "fieldset-label",                           
                           labelWidth : "auto",
                           labelWrap  : true,
                           labelAlign : "right",
                           name       : filter_data[i]['name'],
                           label      : Loc.t("others." + label),                       
                           checked    : filter_data[i]['value'] == filter_data[i]['options'][a]['value'] ? true : false,
                           value      : filter_data[i]['options'][a]['value']
                        }); 
                    }

                items.push({
                    xtype : "fieldset",
                    cls   : "fieldset",
                    layout : 
                    {
                        type : 'hbox',
                        pack : 'start'                       
                    },                        
                    items : child_items                
                });

                break;
                
                case "select":   
                case "multiselect":

                    var options = []; 
                    var value_str = "";

                    for( var a in filter_data[i]['options'])
                    {
                        if (filter_data[i]['type'] == "multiselect")
                        {
                            if (filter_data[i]['options'][a]['caption'].toLowerCase() != "all")
                            {                            
                                options.push({
                                  text : filter_data[i]['options'][a]['caption'],
                                  value: filter_data[i]['options'][a]['value']
                                }); 
                                value_str += filter_data[i]['options'][a]['value'] + ",";
                            }
                        }
                        else                            
                        {
                            options.push({
                              text : filter_data[i]['options'][a]['caption'],
                              value: filter_data[i]['options'][a]['value']
                            }); 
                            value_str += filter_data[i]['options'][a]['value'] + ",";
                        }
                    }
                    
                    value_str = value_str.substr(0, value_str.length -1);
                
                label = filter_data[i]['label'].trim().replace(reg_replace_space, '_').toLowerCase();
                
                items.push({
                      xtype     : "multiselectfield",                      
                      mode      : filter_data[i]['type'] == "select" ? "SINGLE" : "MULTI",
                      usePicker : false,
                      cls       : "select",
                      inputCls  : "select-input select-bg",
                      labelCls  : "select-bg",
                      labelWidth: "auto",
                      labelWrap : true,
                      name      : filter_data[i]['name'],
                      label     : Loc.t("others." + label),
                      options   : options,
                      value     : filter_data[i]['type'] == "select" ? filter_data[i]['value'] : value_str
                });
                
                break;
            
                case "textbox":                    
                    label = filter_data[i]['label'].trim().replace(reg_replace_space, '_').toLowerCase();
                    
                    items.push({
                          xtype      : "textfield",
                          labelWidth : "auto",                                  
                          labelWrap  : true,
                          name       : filter_data[i]['name'],
                          label      : Loc.t("others." + label),
                          value      : filter_data[i]['value']
                    }); 
                    
                break;

            }

        }    
        
        filter_form_height = ((items.length-1)* 2.5) + 3.75;
        filter_form_top = 5.9;

        Ext.Viewport.add({               
                xtype      : "filter_form",
                id         : "filter_form",
                top:  filter_form_top + 'em',
                style : {
                    width  : "98%",
                    height : filter_form_height + "em",
                    borderRadius : "0",
                    padding : "0px",
                    margin  : "0px 0.9% 0px 1%",                    
                },
                items : items, 
                listeners : {
                    initialize : function () {
                        filter_form = this;
                    }
                }
        }); 
        
        Ext.getCmp("filter_btn").setStyle({
           opacity : "1" 
        });
        
        default_report_filter = convert_form_value_to_get_url();
    }
    
    Ext.Function.defer(function()
    {  
        Ext.Viewport.setMasked(false);
    }, 1000);
}

/**
 * function for convert form data to get url's string.
 * @returns string
 */
function convert_form_value_to_get_url()
{
    if (typeof filter_form == "undefined")
    {
        return false;
    }
    
    var form_values = filter_form.getValues();    
    var get_url = "";    
    
    /**
     * form_values are in array 
     * to Convert array to url query String following loop is needed
     */
    for(var i in form_values)
    {
        if(typeof form_values[i] != "object")
        {            
            get_url += i + "=" + form_values[i] + "&";
        }
        else if( form_values[i] instanceof Array )
        {
           get_url += i +"=" + form_values[i].join(",") + "&";
        }
    }
    
    if (get_url != "")
    {
        get_url = get_url.substring(0, get_url.length - 1);
    }
    return get_url;
}

/**
 * function called whenever carousel swipe
 * @param object btn
 * @returns 0
 */
function carousel_swipe(btn)
{
    switch(btn._itemId)
    {
        case "arrow-left": 
            var len = chart_report[active_report]['iframe'].length - 1 ;
            
            if( active_chart_index > 0)
            {
                active_chart_index--;
                hcarousel.previous();
            }
            else
            {
                active_chart_index = len;
                var i = 0;
                var loop = setInterval(function()
                {
                     hcarousel.next(); 
                     if (i > len)
                         clearInterval(loop);
                     i++;
                },160);
                    
            }
        break;
        
        case "arrow-right": 
            
            var len = chart_report[active_report]['iframe'].length -1;
            
            if ( active_chart_index < len)
            {
                hcarousel.next();
                active_chart_index++;
            }
            else
            {
                active_chart_index = 0;
                var i = 0;
                var loop = setInterval(function()
                {
                     hcarousel.previous(); 
                     if (i > len)
                         clearInterval(loop);
                     i++;
                },160);
            }
                
        break;
        
        case "arrow-down": 
            active_sub_chart_index[active_chart_index]++;
            
            var len = chart_report[active_report]['iframe'][active_chart_index]['iframe'].length -1;
            var tab = hcarousel.getActiveItem();            

            if( active_sub_chart_index[active_chart_index] > len )
            {
                active_sub_chart_index[active_chart_index] = 0;
                
                var i = 0;
                var loop = setInterval(function()
                {
                     tab.previous();
                     if (i > len)
                         clearInterval(loop);
                     i++;
                },160);
            } 
            else 
            {
                tab.next();
            }
        break;
    }
    
    Ext.Function.defer(function()
    {     
        btn.setStyle({
           opacity : "1"
        });
    },100);
}

/**
 * main Container Called for create the Chart View.
 */
function create_chart_view(cls, topitem, items)
{    
    horizontal_chart_info['left'] = {
        xtype : "panel",
        layout : {
          type :"vbox", align: 'start',  pack: 'center'
        },
        
        items : [{     
            xtype : "container",                       
            layout : {
                  type :"vbox", align: 'start',  pack: 'center'
            },
            style : {
                width : "7%", height : "12em", 
                textAlign : "center",  
                marginLeft: "2.2em",
                fontSize : "0.75em",
            },
            items :[
                { 
                  id    : "horizontal-chart-info-left",
                  html  : ""
                }
            ]
        }]
    };
     
    horizontal_chart_info['right'] = {
        xtype : "panel",
        layout : {
          type :"vbox", align: 'end',  pack: 'center'
        },
        
        items : [{     
            xtype : "container",                       
            layout : {
                  type :"vbox", align: 'right',  pack: 'center'
            },
            style : {
                width : "7%", height : "12em", 
                textAlign : "center",  
                marginRight: "2.2em",                
                fontSize  : "0.75em",
            },
            items :[
                { 
                  id    : "horizontal-chart-info-right",
                  html  : Loc.t("CHART_SCREEN." + active_report + ".chart.A.title")
                }
            ]
        }]
    };
     
    chart_arrow['right'] = {
        xtype  : "panel",
        id     : "right-arrow",
        layout : {
           type :"vbox", align: 'end',  pack: 'center'
        },
        items : [{
             xtype : "button", 
             cls   : "button",
             itemId: "arrow-right",
             style : {
                 height : "12.48em",
                 width : "3.875em", 
                 backgroundImage : "url('img/arrow_right_red.png')",
                 backgroundSize :"25% 25%",
                 backgroundPosition : "85% 49%",
                 
             },
             handler : function (btn, event) 
             {     
                btn.setStyle({
                   opacity : "0.5"
                });
                
                carousel_swipe(btn);
             }
        }]       
    };
    
    chart_arrow['left'] = {
        xtype  : "panel",
        id     : "left-arrow",
        layout : {
           type :"vbox", align: 'start',  pack: 'center'
        },
        items : [{                 
                xtype : "button", 
                cls   : "button",
                itemId: "arrow-left",
                style : {
                     height : "12.48em", width : "3.875em", 
                     backgroundImage : "url('img/arrow_left_red.png')",
                     backgroundSize :"25% 25%",
                     backgroundPosition : "20% 50%",
                },
               handler : function (btn, event) 
               {            
                  btn.setStyle({
                        opacity : "0.5"
                  });
                
                  carousel_swipe(btn);
               } 
        }]  
    };
    
    chart_arrow['down'] = {
        xtype  : "panel",
        id     : "down-arrow",
        layout : {
           type :"vbox", align: 'center',  pack: 'bottom'
        },
        items : [{
                xtype : "button", 
                cls   : "button",
                itemId: "arrow-down",
                style : {
                    height : "3.875em", width : "12.48em",
                    backgroundImage : "url('img/arrow_down_red.png')",
                    backgroundSize :"25% 25%",
                    backgroundPosition : "50% 87%",
                },
                handler : function (btn, event) 
                {            
                     btn.setStyle({
                        opacity : "0.5"
                     });

                     carousel_swipe(btn);   
                }                
        }]
    };
    
    var mainItems = [];
    mainItems.push(topitem);
    
    if(chart_items.length > 1)
    {
       mainItems.push( horizontal_chart_info['right']);
       mainItems.push( horizontal_chart_info['left']);            
       mainItems.push( chart_arrow['right']);
       mainItems.push( chart_arrow['left']); 
       mainItems.push( chart_arrow['down']);
    }
    
    mainItems.push({
        xtype : "container",
        layout :{
            type : "fit", align : "bottom", pack : "center"
        },        
        items :[
            getTaskBar("")
        ]
    });
    mainItems.push(items);
    
    return {
        xtype : "container",
        autoDestroy: true,
        id    : "chartView",        
        cls   : cls,
        layout : {
          type : "fit",
          pack : "center",
          align: "middle"
        },   
        defaults : {
          autoDestroy: true,  
        },
        items : mainItems,
        listeners :{
            painted : function ()
            {   
                if(chart_items.length > 1)
                {
                    chart_arrow.length = 0;
                    chart_arrow['left'] = Ext.getCmp("left-arrow");
                    chart_arrow['right'] = Ext.getCmp("right-arrow");
                    chart_arrow['down'] = Ext.getCmp("down-arrow");
                  
                    horizontal_chart_info['left'] = Ext.getCmp("horizontal-chart-info-left");
                    horizontal_chart_info['right'] = Ext.getCmp("horizontal-chart-info-right");
        
                    /**
                     * required trigger we can't write this code at painted listeners
                     * bcoz some time painted block trigger before object creation.
                     */
                    hcarousel = Ext.getCmp("horizontal_carousel");
                    
                    refresh_current_iframe();
                }
                
                /**
                 * for Removeing mask after 20 sec
                 */
                Ext.Function.defer(function()
                {            
                    Ext.Viewport.setMasked(false);
                }, 20000);  
            }
        }
    }
}

/**
 * used for creating filter title not filter form.
 * @param boolean icon_small
 * @param String filter // because in some report we no need filter
 */
function chart_filter(icon_small, filter)
{
    var icon_style, mT;
    
    if(icon_small == false) {
        icon_style = "height:3.5em; width :4.6em;";
        mT = "-0.2em";
    } else {
        icon_style = "height:3.1em; width :4.0em;";
        mT = "0em";
    }
    
    if (typeof filter == "undefined")
        filter = true;
    
    var filter_btn = {
            xtype : "button", 
            cls   : "button",
            id    : "filter_btn",
            style: {                                                                                                 
                height  : "3.0em",
                width   : "7.1875em",
                backgroundImage: "url('" + filter_icon + "')",
                backgroundRepeat: "no-repeat",
                backgroundSize: "100% 100%",                   
                fontSize : "0.875em",
                marginTop : ".3em",
                opacity : "0.75"
            },
            handler: function (btn, event) 
            {
                btn.setStyle({ opacity: ".75" });

                //filter_form.show();                    
                $("#filter_form").css("display", "");
                $("#filter_form").css("-webkit-animation" , "show 1s"); /* Chrome, Safari, Opera */

                Ext.Function.defer(function()
                {                            
                   btn.setStyle({ opacity: 1 }); 
                },50);            
                
            }
        };
    
    
    return {     
        xtype  : "container",        
        id     : "filter_container",
        docked : "top",
        layout : {
          type : "card", pack : "start", align: "start"  
        },
        style  : {
          backgroundColor : "transparent",
          height : "4.0em",
          width  : "98%",
          margin : "1em 1% 0 1%",
          border : "none",
          borderBottom : "0.3125em solid "+ report_color,
        },
        items :[
            {
                xtype  : "container",      
                centered : true,
                style  : {
                    backgroundColor: "#fff",
                    width  : "100%",
                    height : "100%",                    
                    padding: "0.312em 0.6875em 0 1.75em"
                },
                layout :{
                  type : "hbox", pack : "start", align: "start"  
                },
                items: [            
                    {
                        html : "<img src='"+ chart_report[active_report]['icon'] +"' style='"+ icon_style +"'>",
                        style : {
                           marginTop : mT,
                           marginRight : "0.5em"
                        }
                    },
                    {
                        id : "chart_title",
                        html : Loc.t("CHART_SCREEN." + active_report + ".title"),
                        style : {
                            color : report_color,
                            fontSize : "1.22em",
                            padding: "0.7em 0px 0px 0px"
                        }
                    },
                    {
                        xtype : "spacer"
                    },
                    filter == true ? filter_btn : {}
                ]
            }
        ]
    };
}

function set_organization_tree(frame, id)
{
    var above_iframe_div = Ext.getCmp(id).down("container");
    
     above_iframe_div.insert(0, {
            xtype : "container",
            layout : {
                type: "auto", align : "right"
            },
            style : {
                height  : "80px", 
                width : "95%",
                padding : "1em 0 4em 1em"
            },
            items :[
                {
                    xtype : "button",                                        
                    text  : Loc.t("others.collapse"),
                    itemId : "Collapse",
                    style: {
                        height  : "2.5em",
                        width   : "7em",
                        color   : "#fff",
                        backgroundColor : "#D24836",
                        backgroundImage : "none",                                            
                        border : "none"
                    },
                    handler : function (btn, e)
                    {
                        var url;

                        if (btn.getItemId() == "Collapse")
                        {
                            btn.setText(Loc.t("others.expand")); 
                            btn.setItemId("Expand");
                            url = domain + server_path + "rdPage.aspx?rdReport=107_Admin_Organization_Hierarchy_Collapse";
                        }
                        else
                        {
                            btn.setText(Loc.t("others.collapse"));
                            btn.setItemId("Collapse");
                            url =  domain + server_path + "rdPage.aspx?rdReport=107_Admin_Organization_Hierarchy";
                        }

                        url += "&" + defaultQueryString + "&chart-type=Tree&" + "lbxCategory="+user.language;
                        url += "#" + encodeURIComponent(document.location.href);

                        Ext.Viewport.setMasked(true);
                        frame.iframe.dom.src  = url;
                    }
                }
            ]                                    
        });
}
/**
 * create the particular container for single iframe.
 * @param String id
 * @param Array iframe
 * @param String next_chart
 */
function chart_container(id, iframe, next_chart, have_child)
{    
    if(typeof next_chart == "undefined")
        next_chart = "";
    
    var cw = 82, ch = 85, scroll = 0;        
    
    if (iframe.url == "rdPage.aspx?rdReport=107_Admin_Organization_Hierarchy&chart-type=Tree")
    {
        scroll = 1;
    }
    
    return { 
            xtype : "container",
            id    : id,
            autoDestroy: true,            
            layout : {
              type : "fit", pack : "center", align : "center"  
            }, 
            defaults : {
               autoDestroy: true  
            },
            items : [
            {
                xtype   : "container", 
                cls     : "chart-iframe-container",
                centered: true,                
                style : {
                    height : ch + "%", width : cw + "%", 
                    marginTop : "-5%", 
                    backgroundColor : "#fff",
                },
                items : [                   
                   {
                        xtype   : scroll == 0 ?  "iframecomponent" : "iframecomponentscroll",
                        id      : iframe.id, 
                        url     : "",
                        layout  : "auto",
                   }
                ],
                listeners : {
                    painted : function () 
                    {
                        var frame = Ext.getCmp(iframe.id);                          
                        
                        var iframe_height = Ext.getCmp(id).element.dom.offsetHeight * (ch/100);
                        iframe_height = iframe_height - 20;
                        
                         var iframe_width = Ext.getCmp(id).element.dom.offsetWidth * (cw/100);                        
                        iframe_width = iframe_width - 20;
                        
                        /**
                         * custome code only use in Organization Hirechy tree
                         */
                        if (iframe.url == "rdPage.aspx?rdReport=107_Admin_Organization_Hierarchy&chart-type=Tree")
                        {
                            set_organization_tree(frame, id);
                            iframe_height -= 150;
                        }
                                                
                        var web_chart_height = iframe_height;
                        var web_chart_width = iframe_width;
                        
                        /**
                        * we set first chart only
                        */                       
                        var temp =  defaultQueryString + "&lbxCategory=" + user.language + "&w=" + web_chart_width + "&h="+ web_chart_height + "&lt=" + (web_chart_height - 60);
                        
                        if (!default_report_query_string)
                        {
                            default_report_query_string = temp;
                            refresh_current_iframe();
                        }
                        
                        default_report_query_string = temp;
                        
                        frame.setStyle({
                            height : (iframe_height + 10) + "px",
                            width  : (iframe_width + 10) + "px"
                        });
                        
                    }
                }
            },
            { 
                layout : {
                    type : "fit", pack : "center", align : "center"  
                  },
                style : {
                   backgroundColor : "transparent",
                   height : "100%",
                   width  : "100%",                   
                },
                items :[
                    {
                        layout : {
                            type : "vbox", pack : "bottom", align : "center"  
                          },
                        items :[
                            {
                                html : next_chart,
                                style : {
                                    marginBottom : "3em",
                                    fontSize : "0.75em"
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    };
}

function vertical_carousel(id, items) 
{
    return  {
        xtype     : "carousel",
        direction : "vertical",
        id        : id,
        style : {
            backgroundColor : "transparent", 
            height : "100%",                
            width  : "100%"
        },
        items : items
    };
    
}


function horizontal_carousel ()
{
    /***
     * Some Report have only one chart then we have to use container instead of carousel
     */
    if(chart_items.length == 1)
    {
        return {
            xtype : "container",   
            layout : "fit",
            style : {
                backgroundColor : "transparent", 
                height : "98%",
                width  : "98%",
                margin : "1%"
            },
            items : chart_items,
        };
    }
    else
    {
        return {
            xtype : "carousel", 
            direction : "horizontal",                
            items : chart_items, 
            id    : "horizontal_carousel",
            style : {
                backgroundColor : "transparent", 
                height : "98%",
                width  : "98%",
                margin : "1%"
            },
            listeners : {
                activeitemchange : function (tabPanel, tab, oldTab) 
                {  
                    if (typeof tab.id != "undefined" && tab.id != "" &&  typeof active_report != "undefined" && active_report != null && active_report != "")
                    {
                        var iframe_id = tab['id'].split("-");
                        active_chart = iframe_id[0];
                        
                        var text_right = "", title_sub_text = '', text_left = "", chart_arr = chart_report[active_report]['iframe'];
                         
                        for (var i = 0 ; i < chart_arr.length; i++)
                        {
                            if( chart_arr[i]['id'] == active_chart)
                            {   
                                var get_url_str = convert_form_value_to_get_url(); 
                                
                                /** 
                                 * Refresh Iframe
                                 */
                                if (typeof chart_arr[i]['iframe'] == "undefined")
                                {
                                    refresh_chart(chart_arr[i], get_url_str);
                                }
                                else
                                {
                                    for (var a in chart_arr[i]['iframe'])
                                    {
                                        refresh_chart(chart_arr[i]['iframe'][a], get_url_str);
                                    }
                                    
                                    // setting the report title
                                    title_sub_text = Loc.t("CHART_SCREEN." + active_report + ".title")  + " (" + Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(65 + i) + ".title") + ")";
                                    Ext.getCmp('chart_title').setHtml(title_sub_text);
                                }                               
                                                                
                                if( i < chart_arr.length -1)
                                {                                
                                    text_right = Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(66 + i) + ".title");
                                }
                                else
                                {
                                    text_right = Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(65) + ".title");
                                }

                                if( i > 0)
                                {
                                    text_left = Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(64 + i) + ".title");
                                }
                                else
                                {
                                    text_left = Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(64 + chart_arr.length) + ".title");
                                }

                                active_chart_index = i;
                            }
                        }

                       if ( typeof horizontal_chart_info['right'] != "undefined")
                       {
                            horizontal_chart_info['right'].setHtml(text_right);  
                       }

                       if ( typeof horizontal_chart_info['left'] != "undefined")
                       {
                            horizontal_chart_info['left'].setHtml(text_left);
                       }

                     
                       if( typeof chart_report[active_report]['iframe'][active_chart_index]['iframe'] != "undefined")
                       {
                            chart_arrow['down'].show();
                       } 
                       else 
                       {
                            chart_arrow['down'].hide();
                       }

                    }

              },

         }
      };
  }
}
/**
 * sets the Items for chart
 */
function set_chart_items ()
{
    chart_items.length = 0;
    chart_count = 0 ;
    
    for(var a in chart_report[active_report]['iframe'])
    {     
        var iframe = chart_report[active_report]['iframe'][a];

        if(typeof iframe['iframe'] != "undefined")
        {
            have_sub_charts = true;
            
            var sub_items = [];
            for( var i in iframe['iframe'])
            {                
                i = parseInt(i);                    
                
                if( typeof active_sub_chart_index[chart_count] == "undefined")
                    active_sub_chart_index[chart_count] = i;
                
                var next_chart = "";

                if (i < iframe['iframe'].length - 1 )
                {
                    next_chart = Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(65 + chart_count) + ".chart." + String.fromCharCode(65 + i+1) + ".title");
                }
                else
                {
                    next_chart = Loc.t("CHART_SCREEN." + active_report + ".chart." + String.fromCharCode(65 + chart_count) + ".chart.A.title");
                }

                sub_items.push(chart_container(iframe['iframe'][i]['id'] +"-container", iframe['iframe'][i], next_chart, true));
            }   

            chart_items.push(vertical_carousel(iframe.id + "-vertical_carousel", sub_items));
        }
        else
        {
            // it is imp. to give -container with iframe.id//
            chart_items.push(chart_container(iframe.id +"-container", iframe, "", false));
        }
        chart_count++; 
    } 
}

function create_chart_object(view_name)
{   
   Ext.Viewport.setMasked(true);
   active_report = view_name;
   active_chart_index = 0;
   active_sub_chart_index.length = 0;
   have_sub_charts = false;
   report_color = "#F0F0F0";
   default_report_query_string = "";
   
   /**
    * Following function will set chart_items,chart_count for futher processing
    */
   set_chart_items();
    
    switch(chart_report[active_report]['category'])
    {
        case "spend":  
            
            filter_icon = "img/icon/" + user.language + "/filter_spend.png";
            report_color = "#0D61B6";
            
            return create_chart_view(
                "chart-screen-bg-blue",
                chart_filter(false), 
                horizontal_carousel()
            );
        break;
        
        case "usage": 
            filter_icon = "img/icon/" + user.language + "/filter_usage.png";
            report_color = "#19A027";
            
            return create_chart_view(
                "chart-screen-bg-green",
                chart_filter(false), 
                horizontal_carousel()
            );
        break;
        
        case "inventory" :             
            filter_icon = "img/icon/" + user.language + "/filter_inventory.png";
            report_color = "#F09719";
            
            return create_chart_view(
                "chart-screen-bg-yellow",
                chart_filter(true), 
                horizontal_carousel()
            );
            
        break;
        
        case "admin" : 
                        
            if(active_report == "organization_hirechy")
            {
                filter_icon = "img/icon/" + user.language + "/filter_organization.png";
                report_color = "#EA3E0F";
                
                return create_chart_view(
                    "chart-screen-bg-organization",
                    chart_filter(true, false),
                    horizontal_carousel()
                );
            }
            else if(active_report == "billfeed_status")
            {                
                filter_icon = "img/icon/" + user.language + "/filter_billfeed.png";
                report_color = "#D600A4";
                
                return create_chart_view(
                    "chart-screen-bg-billfeed",
                    chart_filter(true, false), 
                    horizontal_carousel()
                );
            }
            else
            {
                filter_icon = "img/icon/" + user.language + "/filter_exchange.png";
                report_color = "#9A0CA8";
                
                return create_chart_view(
                    "chart-screen-bg-exchange",
                    chart_filter(true), 
                    horizontal_carousel()
                );
            }
        break;
    }
    
}
