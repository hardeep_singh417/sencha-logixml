/* 
 * created : 12 March 2014
 * author  : Hardeep Singh
 * project : Logixml
 */

function login_footer_icon (img, w) 
{
    w = (w + 8) / 16;
    
    return {
       xtype : "container",     
       style : {
            height : "3.75em",
            width  : w + "em", 
            //marginRight : mR + "em",
            backgroundImage: 'url("img/login_icon/'+img+ '")',
            backgroundRepeat: "no-repeat",
            backgroundSize: "100% 100%",
            backgroundPosition: "center",
       }
    };
}


/**------------ define view -------------------- */
function get_login_view_object()
{
    var  h = 18, w = 30.75;
    //var opt = get_scale_dimensions( {  h : h, w : w});
    
    
    return {
    xtype : "container" ,
    id : "loginView",
    style : {
      backgroundColor : "transparent"      
    },
    layout : {
      type : "fit",
      pack : "center",
      align: "middle"
    },
    items : [
        {
           xtype : 'bgImg',
           id    : "loginView_bg",
           src  : 'img/1024x768/landscape/login_bg.png',
        },
        {
            xtype : "container", 
            centered : true,           
            items :[{
                    xtype   : "iframecomponent", 
                    id      : "login_frame",   
                    //url     : domain + server_path + "logintablet.aspx?" + defaultQueryString + "&parent_url="+ encodeURIComponent(document.location.href),
                    style   : {
                        width : w + "em",
                        height: h + "em", 
                        //marginLeft : "-" + (w - opt.w) + "px",
                        //marginTop : "-" + (h - opt.h) + "px"
                    },                    
            }],    
        },
        {     
            xtype : "container",
            id    : "login_footer",             
            layout :{
                type : "vbox", pack : "bottom", align : "center"
            },
            style : {
                width : "100%", height : "100%"
            },
            items :[
            {
                layout :{
                    type : "vbox"
                },
                style : {
                  backgroundImage: 'url("img/login_icon/login-footer-bg.png")',
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "100% 100%",
                  backgroundPosition: "center",
                  height  : "6.8125em",
                  width   : "100%",
                  padding : "0.625em 0.625em 0em 0.625em",
                },
                items : [
                    {
                        xtype : "container",
                        layout: {
                            type : "hbox", pack : "start", align : "left"
                        },                     
                        flex  : 1,
                        items :[
                            {
                                xtype : "container",
                                id    : "login_bottom",
                                html  : "<img src='img/login_icon/members.png' height='100%' width='100%'>",
                                height: "1.75em",
                                width : "12.875em",                           
                            }
                        ]
                    },    
                    {
                        xtype : "container",
                        layout: {
                            type : "fit", pack : "center", align : "center"
                        },
                        flex  : 3,
                        items : [
                            {
                                xtype  : "container",
                                layout: {
                                    type : "hbox", pack : "center", align : "center"
                                },
                                scrollable : {
                                    direction : 'horizontal',
                                    useIndicators : false
                                },  
                                
                                items : [
                                    login_footer_icon("airtel.png", 75), 
                                    login_footer_icon("ais.png", 51.5),
                                    login_footer_icon("hong_kong.png", 42.5),
                                    login_footer_icon("ctm.png", 58),
                                    login_footer_icon("globe.png", 65.5), 
                                    login_footer_icon("maxis.png", 52.5),
                                    login_footer_icon("mobifone.png", 64),
                                    login_footer_icon("optus.png", 56),
                                    login_footer_icon("singtel.png", 52.5),
                                    login_footer_icon("sk.png", 62),
                                    login_footer_icon("softbank.png", 69),
                                    login_footer_icon("STC.png", 70),
                                    login_footer_icon("taiwan.png", 75),
                                    login_footer_icon("telkomcel.png", 62.5),
                                    login_footer_icon("telkomsel.png", 61),
                                    login_footer_icon("Viva.png", 67),
                                ]
                            }
                        ]
                    }                
                ]
            }
           ]
        }        
    ],
    listeners: {
        painted: function() {              
            Ext.Viewport.setMasked(false);
            loginView = this;
        },
        activate : function ()
        {
            function load_iframe()
            {
                var cmp = Ext.getCmp("login_frame");

                if(typeof cmp != "undefined")
                {
                    var str = "";
                    console.log(user);
                    if (typeof user.username != "undefined" && typeof user.pwd != "undefined")
                    {
                        str += "&username=" + user.username + "&pwd=" + user.pwd;
                    }

                    cmp.iframe.dom.src = domain + server_path + "logintablet.aspx?" + defaultQueryString + str + "&parent_url="+ encodeURIComponent(document.location.href);
                }
            }
            
            if (check_connection)
            {   
                check_internet_connection({
                    successFn : function ()
                    {
                        load_iframe();
                    },
                    failureFn : function ()
                    {
                        var cmp = Ext.getCmp("login_frame").getParent();

                        if(typeof cmp != "undefined")
                        {
                            cmp.removeAll();
                            cmp.add([
                                connection_failed_template({ h: "13em", w : w + "em"})
                            ]);
                        }
                    }
                });
            }
            else
            {
                load_iframe();
            }
        }
    }

};
}
