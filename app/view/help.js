/* 
 * created 23 Apr 2014
 * @Project : Logixml
 * @author : Hardeep singh
 */
var helpView;

function set_help_view_object()
{
  helpView = {
  id : "helpView",
  xtype : "container", 
  cls : "chart-screen-bg-blue",
  layout : {
      type : "vbox", align : "center", pack : "start"
  },    
  items : [
      get_view_header_object("#539AD6", Loc.t("HELP_SCREEN.title"), "img/help-blue.png"),
      {
          xtype : "container",           
          layout : {
                type : "vbox", align : "center", pack : "center"
          },     
          style : {
                width : "98%", height : "100%"
          },
          items : [
              {
                    xtype : "container",  
                    centered: true,
                    layout : {
                        type : "fit", align : "center", pack : "center"
                    },
                    style : {
                        width : "80%", height : "90%",
                    },
                    items :[
                        {
                            xtype : "container",
                            cls   : "content",
                            layout : {
                                  type : "fit", align : "start", pack : "start"
                            },                            
                            style : {
                                width : "100%", height : "100%",
                            },
                            scrollable : {
                                  direction : 'both',
                                  useIndicators : true
                            },
                            styleHtmlContent : true,
                            items : [
                                {
                                    html : Loc.t("HELP_SCREEN.content")
                                }
                            ]
                        }
                    ]
             },
             getTaskBar("help")
          ]
      }
  ]
};

}


