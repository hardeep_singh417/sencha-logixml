/* 
 * created 23 Apr 2014
 * @Project : Logixml
 * @author : Hardeep singh
 */

function get_setting_view_object () 
{
  var h = 19.6875, w = 42.875;
  //var opt = get_scale_dimensions( {  h : h, w : w});
    
  return  {
  id : "settingView",
  xtype : "container", 
  cls : "chart-screen-bg-blue",
  layout : {
      type : "vbox", align : "center", pack : "start"
  },
  items : [
      get_view_header_object("#539AD6", Loc.t("SETTING_SCREEN.title"), "img/setting-blue.png"),
      {
          xtype : "container",           
          layout : {
                type : "vbox", align : "center", pack : "center"
          },     
          style : {
                width : "98%", height : "100%"
          },
          items : [
              {
                    xtype : "container",  
                    cls : "content",
                    centered: true,
                    layout : {
                        type : "fit", align : "center", pack : "center"
                    },
                    style : {
                        width : "80%", height : "95%",
                    },
                    items : [              
                        {
                              xtype : "container",
                              layout : {
                                 type : "auto", align : "center", pack : "center"
                              },      
                              autoDestroy : true,
                              items :[
                                  {
                                       xtype   : "iframecomponent",    
                                       id      : "setting_iframe",                       
                                       centered : true,
                                       autoDestroy : true,
                                       style   : {
                                            width : w + "em",
                                            height: h + "em"
                                        },
                                  }
                              ]

                      }
                  ]
             },
             getTaskBar("setting")
          ]
      }     
  ],
  listeners: {        
        activate : function ()
        {
           function load_iframe()
           {
                var cmp = Ext.getCmp("setting_iframe");
                cmp.iframe.dom.src = domain + server_path + "rdPage.aspx?rdReport=01_Settings&language=" + user.language + "&lbxCategory="+ user.language + "&" + defaultQueryString +"&parentURL="+ encodeURIComponent(document.location.href);
           }
           
           if (check_connection)
            {   
                check_internet_connection({
                    successFn : function ()
                    {
                        load_iframe();
                    },
                    failureFn : function ()
                    {
                        var cmp = Ext.getCmp("setting_iframe");

                        if(typeof cmp != "undefined")
                        {
                            cmp = cmp.getParent().getParent();
                            cmp.removeAll();
                            cmp.add([
                                connection_failed_template({ h: "100%", w : "100%", cssClass : ""})
                            ]);
                        }
                    }
                });
            }
            else
            {
                load_iframe();
            }
        }
    }
  
};

}
