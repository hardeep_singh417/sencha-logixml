/* 
 * created : 12 March 2014
 * author  : Hardeep Singh
 * project : Logixml
 */

var tile_clicked = false;

/**
* function for removing from mainView
*/    
function remove_mainView_items(len)
{
     var i;     
     var inner_items = mainView.getInnerItems();    
     while ( inner_items.length > len)
     {   
         i = inner_items.length - 1; 
         mainView.remove(inner_items[i], true);    
         inner_items.splice(i, 1);        
     }
}

function dashboard_listener_call(btn, event)
{
    last_taskbar_btn = "";
    
     /**
    * removeing from Viewport
    */
    var i, inner_items = Ext.Viewport.getItems().items; 
    
    /**
     * we must remove existing chart object before loading next chart object
     * 0 = login,  1 = mainview
     */
    while ( inner_items.length > 2)
    {   
        i = inner_items.length - 1; 
        Ext.Viewport.remove(inner_items[i], true);            
        inner_items.splice(i, 1);        
    }
    
    /**
     * condition if user double click the tile
     */
    if (!tile_clicked)
    {
        tile_clicked = true;
    }
    
    /**
     * removing from mainView
     */
     remove_mainView_items(1);

     Ext.Function.defer(function()
     { 
         if ( mainView != "undefined" && mainView != null && typeof Ext.getCmp("chartView") == "undefined")
         {
             mainView.add(create_chart_object(btn._itemId));  
             mainView.animateActiveItem(1,{ type : 'slide',   direction : "left"  });
         }             

        var op = btn._style.opacity;     
        btn.setStyle({ opacity: op}); 
        tile_clicked = false;
     },100); 
}

function dashborad_large_label(img, ori)
{
    return {
        xtype : "container",   
        style : {          
          height : "4.875em",
          width : ori == "landscape"? "30.8125em" : "22.8125em",
          backgroundImage : "url('" + img + "')",
          backgroundPosition : "center",
          backgroundSize : "100% 100%",
          backgroundRepeat : "no-repeat",
          marginBottom : "1.25em"
        },
        
    };
}

function get_widget_btn (id, img)
{
    return {
        xtype   : "button",        
        itemId  : id,
        style : {              
              width  : "6.75em",
              height : "6.75em",                            
              backgroundImage: 'url("'+img+ '")',
              backgroundRepeat: "no-repeat",
              backgroundSize: "100% 100%",
              backgroundPosition: "center",
              borderRadius : "0",
              opacity : ".9",
              border : "none"
          },
        handler : function (btn, event) 
        {            
           dashboard_listener_call(btn, event);            
        }
    };
}

function dashboard_widget_btn(itemfunctions )
{
    return {
        xtype : "container",
        layout : {
            type : "hbox",
            pack : "start",
            align: "start"
        },       
        items : itemfunctions
    };
}

function get_large_widget_btn(id, img)
{
    return {
        xtype : "button",                
        itemId: id,
        style : {              
              width  : "14.75em",
              height : "5.4em",
              backgroundImage : 'url("'+img+ '")',
              backgroundRepeat: "no-repeat",
              backgroundSize  : "100% 100%",
              backgroundPosition: "center",
              borderRadius : "0",
              opacity : ".9",
              border : "none"
          },
        handler : function (btn, event) 
        {
           dashboard_listener_call(btn, event); 
        }
    };
}

function dashboard_large_widget_btn(itemfunctions)
{
    return {
        xtype : "container",
        style : {
          width: "100%",
          height : "7.5em"          
        },
        layout : {
            type : "hbox",
            pack : "start",
            align: "start"
        },       
        items : itemfunctions
    };
}


function set_dashboard()
{
    
    dashboard_landscape = {    
        xtype : "container",
        itemId : "dashboard_container", 
        layout : {
            type : "vbox",
            pack : "start",
            align: "start"
          },
        style : {
           backgroundColor : "transparent",
           height : "auto",
           marginLeft : "0.3125em",
           marginRight: "0.3125em"
        },     
        items :[
            {
                //spend and usage                       
                layout : {  type : "hbox",  pack : "start",   align: "start" },
                items : [
                    {
                        style: {
                            marginRight: "0.625em",  marginTop: "0.625em"
                        },
                        layout : {  type : "vbox",  pack : "start",   align: "start" },
                        items : 
                        [
                            dashborad_large_label(tile_path + "landscape/spend.png", "landscape"),
                            dashboard_widget_btn([
                                get_widget_btn("spend_month", tile_path + "month_blue.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("spend_country", tile_path + "country_blue.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("spend_charge_type", tile_path + "charge_type.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("spend_cost_center", tile_path + "cost_center.png"),
                            ]),
                            { style : {height  : "1.25em"}},
                            dashboard_widget_btn([                            
                                get_widget_btn("spend_top_spender",tile_path + "top_spender_blue.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("spend_top_international",tile_path + "top_international_blue.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("spend_top_roaming",tile_path + "top_roaming_blue.png")
                            ])
                        ]
                    },
                    {
                        style: {
                            marginLeft: "0.625em", marginTop: "0.625em"
                        },
                        layout : {  type : "vbox",  pack : "start",   align: "start" },
                        items : 
                        [
                            dashborad_large_label(tile_path + "landscape/usage.png", "landscape"),
                            dashboard_widget_btn([
                                get_widget_btn("usage_month", tile_path + "month_green.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("usage_country", tile_path + "country_green.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("usage_top_user", tile_path + "top_spender_green.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("usage_top_international", tile_path + "top_international_green.png")
                            ]),
                            { style : {height  : "1.25em"}},
                            dashboard_widget_btn([   
                                get_widget_btn("usage_top_roaming", tile_path + "top_roaming_green.png"),{ style : {width  : "1.25em"}},
                                get_widget_btn("usage_long_calls", tile_path + "long_calls.png"),
                            ])

                        ]
                    }
                ]
            },
            {
                //large 4 icons
                xtype : "container",
                layout : {
                    type : "auto", pack : "center", align : "center"
                },
                style : {                
                    width : "100%"
                },            
                items : [ 
                    { 
                        style : { height : "6.5em"}
                    },
                    {
                        xtype : "container",
                        layout : {
                           type : "hbox", pack : "center", align : "center"
                        },
                        items :[
                               dashboard_large_widget_btn([                   
                                 get_large_widget_btn("inventory_report", tile_path + "inventory.png"), { style : {width  : "1.25em"}},
                                 get_large_widget_btn("organization_hirechy", tile_path + "organization.png"), { style : {width  : "1.25em"}},
                                 get_large_widget_btn("billfeed_status", tile_path + "bill.png"), { style : {width  : "1.25em"}},
                                 get_large_widget_btn("exchange_rates", tile_path + "exchange.png")
                             ])
                        ]
                    }
                ]
            },

        ],
        listeners : {                
            painted : function () {
                dashboard_loaded = true;
            }
        }
    };

}