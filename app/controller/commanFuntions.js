/* 
 * created : 12 March 2014
 * Author  : Hardeep Singh
 * Project : Logixml
 */

function defineComponents()
{
    Ext.define('IframeComponent', {
        extend: 'Ext.Component',
        xtype: 'iframecomponent',
        config: {                       
            url     : null                
        },
        initialize: function() {
            var me = this;            
            me.callParent();
            
            me.iframe = this.element.createChild({
                tag   : 'iframe',
                src   : this.getUrl(),
                style : 'width: 100%; height: 100%; border:none; ',
                scrolling: "no"
            });

            me.relayEvents(me.iframe, '*');
        },
        painted : function ()
        {
          
        }        
    });

    Ext.define('IframeComponentScroll', {
        extend: 'Ext.Component',
        xtype: 'iframecomponentscroll',
        config: {                       
            url     : null                
        },
        initialize: function() {
            var me = this;
            me.callParent();
                
            me.iframe = this.element.createChild({
                tag   : 'iframe',
                src   : this.getUrl(),
                style : 'width: 100%; height: 100%; border:none; ',
                scrolling: "yes"
            });

            me.relayEvents(me.iframe, '*');
        }
    });
    
    
    Ext.define("logixml.body", {
         extend: 'Ext.Container',
         xtype : "bodyView",
         config: {
            fullscreen : true,
            cls : "full_bg",
            height: '100%',
            width: '100%',
            layout : { 
                type: 'card',
                animation : { type : "slide", duration : 500}
            },             
        },
        initialize: function () 
        {
            bodyView = this;
        }
    });
    
    
    Ext.define("logixml.bgimg", {
         extend: 'Ext.Img',
         xtype : "bgImg",
         config: {
            cls : "full_bg",
            height: 'auto',
            width: '100%',
            layout : 'fit',            
        }
    });
    
    
    Ext.define("logixml.mainView", {
         extend: 'Ext.Container',
         xtype : "mainView",
         config: {
            layout : 'fit'
        }
    });
    
    
    Ext.define("logixml.filter_form", {
         extend: 'Ext.form.Panel',
         xtype : "filter_form",
         config: {
            cls        : "filter_form",
            fullScreen : false,
            hidden     : true,
            floating   : true,
            enableSubmissionForm : false,
            top: '5.9em',
            showAnimation: {
                type: 'slideIn',
                duration: 500,
                direction:'down',
                easing : "ease-out"
            },
            hideAnimation: {
                type: 'slideOut',
                duration: 500,
                direction:'up',
                easing : "ease-out"
            },
           
            layout : {
                type : "auto"
            },
            defaults : {
                autoDestroy : true
            } 
         }
        
    });
    
}

function connection_failed_template(opt)
{
    opt.cssClass = typeof opt.cssClass == "undefined" ? "content" : opt.cssClass;
    
    Ext.Viewport.setMasked(false);  
    
    return {
        xtype : "container",                    
        style : {          
          backgroundColor : "#ccc"
        },
        layout : {
          type : "vbox",
          pack : "center",
          align: "middle"
        },    
        style : {
            width : opt.w, height : opt.h,
        },
        items : [                    
            {
                xtype : "container",
                cls   : opt.cssClass,
                layout : {
                      type : "vbox", align : "start", pack : "start"
                },                            
                style : {
                    width : "100%", height : "100%",
                    fontSize : "1.4em",
                },
                scrollable : {
                      direction : 'both',
                      useIndicators : true
                },
                styleHtmlContent : true,                
                items : [
                    {
                        html : "There is no internet connection available. Please ensure that you are connected to Internet to use the application",
                        style : {
                            color : "#D33F2A"
                        }
                    },
                    {
                        xtype : "button", 
                        cls   : "button",    
                        text  : "Reload",
                        style: {                                                                                                 
                            height  : "3.0em",
                            width   : "7.1875em",                            
                            fontSize : "1em",
                            marginTop : "0.3em",
                        },
                        handler: function (btn, event) 
                        {
                              Ext.Viewport.setMasked(true);
                              window.location.reload();                            
                        }
                    }
                ]
            },           
        ],
    };    
}

function check_internet_connection(args)
{
    // check internet connection
    $.ajax({
        type: 'GET',
        url: domain + server_path + "rdPage.aspx?rdReport=01_Header&lbxCategory=English",
        timeout: 5000,
        success: function(data) {
            args.successFn();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) 
        {
            args.failureFn();
        }
    });

}

/**----------- define Constants variables ---------------- */

/**
 *  Defining Domain
 *  Following Constant required in whole application.
 */
var domain = "http://testcop.bridgealliance.com";

/**
 * this variable refer to path of location of project on server
 * this path can't cancate with domain because domain is used for XD postmessaging.
 */
var server_path = "/TSM_Dev/";

/**
 * Default Query string of All Iframes. Every request contain the query string
 * Never include '&' in start and end point of string.
 */
var defaultQueryString = "rdMobile=True";


/**
 * defining other varables 
 */
var img_path = "img/1024x768/", tile_path = "img/tiles/"; 
var mainView, dashboardView, dashboard_landscape, dashboard_portrait, chart_loaded = true;
var user, zoom;
var loginView, header, bodyView;
var scale;

/**
 * This is Language Parameter object Logixml Lng : Sencha Lng
 * value of array is sencha languages. it is files located at /locales/ * 
 */
var lang_arr = {
    "English" : "en",
    "SimplifiedChinese" : "ch",
    'Japanese' : "jp",
    'Arabic'  : "ar",
    'Korean'  : "kr",
    'Russian' : "ru",
    'French'  : "fr"    
};

/**
 * object of background images  BMO : image name  
 */
var bmo_arr = {
    'Airtel' : "red.jpg",
    'Telkomsel' : "red.jpg",
    'SK Telecom' : "red.jpg",
    
    'SingTel' : "singtel.jpg",    
    
    'AIS' : "green.jpg",
    'Maxis' : "green.jpg",    
    
    'CSL' : "csl.jpg",    
        
    'Optus' : "optus.jpg",
    
    'CTM' : "blue.jpg",    
    'SoftBank' : "blue.jpg",    
    'Taiwan Mobile' : "blue.jpg",
    'Globe' : "blue.jpg",
    'MobiFone' : "blue.jpg",
    
    'STC' : "default.jpg",
    'BridgeAlliance' : "default.jpg",
};

/**
 * following variable used for testing in desktop. please set it false while testing in desktop
 * and set it true while deploying
 */
var check_connection = false;

/**
 * function for post messaging for chart screen iframes
 */

XD.receiveMessage(function(response)
{    
    if(typeof response.data == "object")
    {   
        taskbar_click = true;                
        
        if (typeof mainView == "undefined")
        {
            // removing the body view 's dashboard 
            if (bodyView.getInnerItems().length > 1)
            {
                bodyView.removeAt(1);
            }
            
            /**
             * Post message comes from Login Screen on login successful
             */
            Ext.Viewport.setMasked(true);
            
            set_general_on_login(response);            
            
            // after set tile_path we can call following.
            set_dashboard();
            
            dashboardView = {     
                xtype : "container",    
                id    : "dashboardView",
                cls    : "full_bg",
                style : {
                  background : 'url('+ get_main_view_img()+') no-repeat',
                  backgroundColor : "#83C1EB"                
                },
                layout : {
                  type : "vbox",
                  pack : "center",
                  align: "middle"
                },                   
                items : [                    
                    dashboard_landscape,
                    getTaskBar("home")
                ],
                listeners :{
                    activate : function () {
                        Ext.Viewport.setMasked(false);
                    },
                    painted : function () {
                        dashboardView = this;
                    }
                }
            };
            
            /**
             * using  bodyView.setActiveItem(create_main_view(dashboardView)) instead of bodyView.setActiveItem(create_main_view(dashboardView))
             * This is beacuse some app take lot time to load images which hinder the animation so we have add first then animate
             */
            bodyView.add(create_main_view(dashboardView));
            
            Ext.Function.defer(function()
            {            
                bodyView.setActiveItem(1);
            },500);
        }
        else
        {
            switch(mainView.getActiveItem().id)
            {
                case "settingView" :
                    if (typeof response.data.language != "undefined")
                    {
                        user.language = response.data.language.trim();                          
                        // set localstore Language
                        set_user();                                               
                        Loc.load_language(user.local_language);                    
                        tile_path = "img/tiles/" + user.language  + "/";
                        
                        // please put it before update Dashboard
                        remove_mainView_items(1);
                        // after set tile_path we can call following.
                        update_dashboard();
                        
                        update_static_iframes();   
                        set_help_view_object();
                    }
                    
                break;
                
                case "chartView" :                    
                    Ext.Viewport.setMasked(true);
                    set_filter_component(response.data.filter);
                break;
                
                /// Re login
                default : 
                    /**
                    * Following statement must excute if user re-login.
                    * each and every iframe must refrsh so that actual information can be seen.
                    */     
                   remove_mainView_items(1);  
                   set_general_on_login(response);
                   
                   update_dashboard();
                   update_static_iframes();                   
                   set_help_view_object();
                   
                   /**
                    * set mainView Bg image
                    */
                    var cmp = Ext.getCmp("mainView_bg");
                
                    if(typeof cmp != "undefined")
                    {
                        cmp.setSrc(get_main_view_img());
                    }
                   
                    bodyView.animateActiveItem(1, 
                    {
                        type : 'slide',
                        direction : "left"
                    });
                    
               break;
            }
        }
    }    
},domain);


/**
 * comman function for creating header in help and setting.
 */
function get_view_header_object(color, txt, img)
{
    return {     
        xtype  : "container",        
        docked : "top",
        layout : {
          type : "vbox", pack : "start", align: "start"  
        },
        style  : {
          backgroundColor : "transparent",
          height : "4.0em",
          width  : "98%",
          margin : "1em 1% 0 1%",
          border : "none",
          borderBottom : "0.3125em solid "+ color,
        },
        items :[
            {
                xtype  : "container",                
                style  : {
                    backgroundColor: "#fff",
                    width : "100%",
                    height : "100%",                    
                    padding: "0 0 0 1.75em"
                },
                layout :{
                  type : "hbox", pack : "start", align: "start"  
                },
                items: [            
                    {                      
                        style :{
                            height : "2.8em",
                            width : "2.6875em",
                            backgroundImage : "url('"+ img+ "')",
                            backgroundPosition : "center",
                            backgroundSize : "100% auto",
                            backgroundRepeat : "no-repeat",
                            marginTop : "0.5em",
                            marginRight : "1em"
                        }
                    },
                    {
                        html : txt,
                        style : {
                            color : color,
                            fontSize : "1.6em",
                            padding: "0.6em 0px 0px 0px"
                        }
                    }                    
                ]
            }
        ]
    };
}


function set_general_on_login(response)
{
    if( typeof response.data.username != "undefined")
    {
        user.username = response.data.username.trim();
    }
    
    if( typeof response.data.pwd != "undefined")
    {
        user.pwd = response.data.pwd.trim();
    }
    
    if (typeof response.data.bmo != "undefined")
    {
        user.bmo = response.data.bmo.trim();
    }
  
    set_user();
    
    Loc.load_language(user.local_language);
    tile_path = "img/tiles/" + user.language + "/";   
}

function update_dashboard()
{
    set_dashboard();
                    
    dashboardView.removeAll();
    Ext.Function.defer(function()
    {
        dashboardView.add(dashboard_landscape);
        mainView.animateActiveItem(0, 
        {
            type : 'slide',
            direction : "left"
        });

        Ext.Viewport.setMasked(false);
    },100);

}

function update_static_iframes()
{
    var header = Ext.getCmp("header"), src = header.iframe.dom.src;
    header.iframe.dom.src = src;
}


function get_scale_iframe_style()
{
    return {
        "-ms-zoom": zoom,
        "-moz-transform" : "scale(" + zoom + ")",
        "-moz-transform-origin": "0 0",
        "-o-transform" :  "scale(" + zoom + ")",
        "-o-transform-origin" : "0 0",
        "-webkit-transform":  "scale(" + zoom + ")",
        "-webkit-transform-origin": "0 0",
    };
}
