/* 
 * created : 19 May 2014
 * author  : Hardeep Singh
 * project : Logixml
 */

var localstore, user = {};

function load_model_store()
{    
    /**
    * for user language
    */
   Ext.define('myModel', {
       extend: 'Ext.data.Model',
       config: {	        
       }
   });

   localStore = Ext.create('Ext.data.Store', 
   { 
       model : "myModel",
       storeId: 'userstore'          
   });
        
}


function set_user ()
{
    var obj = {};
    
    if (user && typeof user.username != "undefined")
    {
        obj.username = user.username;
    }

    if (user && typeof user.language != "undefined")
    {
        obj.language = user.language
    }
    else
    {
        obj.language = "English";
    }

    if (user && typeof user.pwd != "undefined")
    {
        obj.pwd = user.pwd
    }

    if (user && typeof user.bmo != "undefined")
    {
        obj.bmo = user.bmo
    }
    
    user = obj;
    
    user.local_language = lang_arr[user.language];

    localStorage.setItem("user", JSON.stringify(user));
}

function get_user ()
{
    user = JSON.parse(localStorage.getItem("user"));    
    if(typeof user == "object" && user != null && typeof user["language"] != "undefined")
    {
        user.local_language = lang_arr[user.language];
    }
    else
    {
        set_user();
    }
}