/**
 @classes singleton class which holds all translated static text in English
 Ext.ux.Localization determines language and than uses corresponding file from folder locales
 @example text: Loc.t('DASHBOARD_SCREEN.LABELS.SPEND')

    @package    Logixml
    @copyright  Copyright @ 2014
    @created    8 May 2014
    @author     Hardeep singh
 */

/**
 * Imp : Name of file and 'locales.en' should be same
 */

Ext.define('locales.en', {
    extend: 'Ext.Base',

    singleton: true,        
   CHART_SCREEN : {
        spend_month : {           
            title : "Spend Summary by Month",
            chart : {
                A : { title : "Total Spend" },
                B : { title : "Average Spend per Line" },
            }            
        },
        spend_country : {           
            title : "Spend Summary by Country",
            chart : {
                A : { title : "Total Spend"},
                B : { title : "Average Spend per line vs. No of lines"}
            }     
        },
        spend_charge_type : {           
            title : "Spend Summary by Charge Type",
            chart : {
                A : { title : "Total Spend"},
                B : { title : "Average Spend per Line"}
            }     
        },
        spend_cost_center : {           
            title : "Spend Summary by Cost Center",
            chart : {
                A : { title : "Average Spend per line vs. No of lines"},
                B : { title : "Average Spend per line vs. No of lines"}
            }     
        },
        spend_top_spender : {           
            title : "Spend Summary by Top Spender",
            chart : {
                A : { title : "Top Users Total Spend Over Selected Period"},
                B : { title : "Country Location of High Spending Users (by % of users)"}
            }     
        },
        spend_top_international : {           
            title : "Spend Summary by Top International Destination",
            chart : {
                A : { title : "Top International Traffic Spend"},
                B : { title : "Top International Traffic Spend by %"}
            }     
        },
        spend_top_roaming : {           
            title : "Spend Summary by Top Roaming Destination",
            chart : {
                A : { title : "Roaming Spend in Top Visited Countries"},
                B : { title : "Roaming Spend in Top Visited Countries by %"}
            }
        },
        
        usage_month : {
            title : "Usage Summary by Month",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line" }
                    }
                },
                B :{
                    title : "Data",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line" }
                    }
                },
                C :{
                    title : "Messaging",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line" }
                    }
                }
            }
        },
        usage_country : {
            title : "Usage Summary by Country",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line (Voice) Vs No. of Lines" }
                    }
                },
                B: {
                    title : "Data",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line (Data) Vs No. of Lines" }
                    }
                },
                C : {
                    title : "Messaging",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line (Messaging) Vs No. of Lines" }
                    }
                }              
            }
        },        
        usage_top_user : {
            title : "Usage Summary by Top User",
            chart : {
                A : { title : "Total Usage (of selected type) by Top High Volume Users" },
                B : { title : "Country Location of Top High Volume Users" }                              
            }
        },      
        usage_top_international : {
            title : "Usage Summary by Top International",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A : { title : "Top International Call Usage" },
                        B : { title : "Top Destinations for International Call Usage by %" }
                    }
                },
                B : {
                    title : "Messaging",
                    chart : {
                        A : { title : "Top International Messaging Usage" },
                        B : { title : "Top Destinations for International Messaging Usage by %" }
                    }
                }                         
            }
        },
        usage_top_roaming : {
            title : "Usage Summary by Top Roaming",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A: { title : "Top Roaming Calls (Minutes)" },
                        B: { title : "Top Destinations for Roaming Calls" }
                    }
                },
                B : {
                    title : "Messaging",
                    chart : {
                        A : { title : "Top Roaming Message (Nos)" },
                        B : { title : "Top Destinations for Roaming Message Usage" }
                    }
                },
                C : {
                    title : "Data",
                    chart : {
                        A : { title : "Top Roaming Data(MB)" },
                        B : { title : "Top Destinations for Roaming Data Usage" }
                    }
                }               
            }
        },
        usage_long_calls : {
            title : "Usage Summary by Long Calls",
            chart : {
                A : { title : "Total Long Duration Calls (Minutes)" },
                B : { title : "Country Location of Top Long Duration Calls" } 
            }
        },
        inventory_report :{
            title : "Inventory"
        },
        billfeed_status :{
            title : "Billfeed Status"
        },
        organization_hirechy : {
            title : "Organization Hierarchy",
            chart : {
                A : { title : "Organization Hierarchy Tree"},
                B : { title : "List of Unassigned Numbers"}
            }
        },
        exchange_rates : {
            title : "Exchange Rates",
            chart : {
                A: { title : "US$ to Local Currency" }
            }
        },         
    },
    HELP_SCREEN : {
        title : "Help",
        content : "<p>Welcome to  Telecom Spend Manager Tablet App!</p><p>Get an overview of your mobile telecom spending and usage across the region. This app presents the same information as the dashboard of your TSM desktop web application.</p><p>When in a screen, tap on the arrows (left/right or bottom) to display other graphs related to the same metrics. Tap on the graph to make appear some detailed information. Tap on the Filter button on top right of the screen to change the filter parameters of your graph. Tap on the legend (where applicable) to filter some of the value.</p><p>Use the filters on top-right of the screen to modify the range of data displayed.</p>"
    },
    SETTING_SCREEN : {
        title : "Setting"
    },
    others : {        
        select_country : "Select Country",
        select_period : "Select Period",
        organized_by_country : "Organized by Country",
        organized_by_charge_type : "Organized by Charge Type",
        select_charge_type : "Select Charge Type",
        organized_by_month : "Organized by Month",
        select_number_of_destination : "Select Number of Destination",
        select_a_report_type : "Select a Report Type",
        select_contributing_country : "Select Contributing Country",
        select_type_of_usage : "Select Type of Usage",
        select_no_of_users : "Select No of Users",
        select_type_of_traffic : "Select Type of Traffic",
        select_number_of_top_long_duration_call : "Select Number of Top Long Duration Call",
        select_destination : "Select Destination",
        local_currency_to_us$ : "Local Currency to US$",
        us$_to_local_currency : "US$ to Local Currency",
        select_number_of_users : "Select Number of Users",
        expand : "Expand",
        collapse : "Collapse"
    }
});