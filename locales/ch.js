/**
 @classes singleton class which holds all translated static text in English
 Ext.ux.Localization determines language and than uses corresponding file from folder locales
 @example text: Loc.t('DASHBOARD_SCREEN.LABELS.SPEND')

    @package    Logixml
    @copyright  Copyright @ 2014
    @created    25 Sep 2014
    @author     Hardeep singh
 */

Ext.define('locales.ch', {
    extend: 'Ext.Base',

    singleton: true,
    
    CHART_SCREEN : {
        spend_month : {           
            title : "每月开销摘要",
            chart : {
                A : { title : "总消费" },
                B : { title : "每行的平均消费" },
            },            
        },
        spend_country : {           
            title : "各别国家开销摘要",
            chart : {
                A : { title : "总消费"},
                B : { title : "平均花费每行与行号"}
            }     
        },
        spend_charge_type : {           
            title : "各别費用类型开销摘要",
            chart : {
                A : { title : "总消费"},
                B : { title : "每行的平均消费"}
            }     
        },
        spend_cost_center : {           
            title : "各别企業成本中心开销摘要",
            chart : {
                A : { title : "平均花费每行与行号"},
                B : { title : "平均花费每行与行号"}
            }     
        },
        spend_top_spender : {           
            title : "按热门富豪花摘要",
            chart : {
                A : { title : "顶级用户选择期间总花费超过"},
                B : { title : "国家的高消费用户位置（％的用户"}
            }     
        },
        spend_top_international : {           
            title : "由国际顶级花摘要",
            chart : {
                A : { title : "国际顶级交通支出"},
                B : { title : "国际顶级交通花费％"}
            }     
        },
        spend_top_roaming : {           
            title : "按热门漫游花摘要",
            chart : {
                A : { title : "在顶部访问的国家漫游的支出"},
                B : { title : "由漫游％花在热门访问的国家"}
            }
        },
        
        usage_month : {
            title : "按月份用法总结",
            chart : {
                A : {
                    title : "通话",
                    chart : {
                        A : { title : "总使用量" },
                        B : { title : "平均每行的用法" }
                    }
                },
                B :{
                    title : "数据",
                    chart : {
                        A : { title : "总使用量" },
                        B : { title : "平均每行的用法" }
                    }
                },
                C :{
                    title : "消息",
                    chart : {
                        A : { title : "总使用量" },
                        B : { title : "平均每行的用法" }
                    }
                }
            }
        },
        usage_country : {
            title : "各别国家使用量摘要",
            chart : {
                A : {
                    title : "通话",
                    chart : {
                        A : { title : "总使用量" },
                        B : { title : "平均每行的用法（语音）VS号线" }
                    }
                },
                B: {
                    title : "数据",
                    chart : {
                        A : { title : "总使用量" },
                        B : { title : "平均每行的用法（数据）VS号线" }
                    }
                },
                C : {
                    title : "消息",
                    chart : {
                        A : { title : "总使用量" },
                        B : { title : "平均每行的用法（消息）VS号线路" }
                    }
                }              
            }
        },        
        usage_top_user : {
            title : "按热门用户使用情况摘要",
            chart : {
                A : { title : "顶高容量用户的总使用量（选择类型）" },
                B : { title : "国家顶高容量用户的位置" } 
            }
        },      
        usage_top_international : {
            title : "由国际顶级的用法总结",
            chart : {
                A : {
                    title : "通话",
                    chart : {
                        A : { title : "国际顶级呼叫用途" },
                        B : { title : "最热门的目的地％的国际长途电话使用"  }
                    }
                },
                B : {
                    title : "消息",
                    chart : {
                        A : { title : "国际消息用法" },
                        B : { title : "最热门的目的地为国际通讯用途％" }
                    }
                }                         
            }
        },
        usage_top_roaming : {
            title : "按热门漫游使用情况摘要",
            chart : {
                A : {
                    title : "通话",
                    chart : {
                        A: { title : "总漫游通话时间（分钟）" },
                        B: { title : "最热门的目的地使用漫游通话" }
                    }
                },
                B : {
                    title : "消息",
                    chart : {
                        A : { title : "最热门的漫游消息（中）" },
                        B : { title : "最热门的目的地使用漫游消息" }
                    }
                },
                C : {
                    title : "数据",
                    chart : {
                        A : { title : "最热门的漫游数据（千字节）" },
                        B : { title : "最热门的目的地漫游数据使用" }
                    }
                }               
            }
        },
        usage_long_calls : {
            title : "由长话费使用情况摘要",
            chart : {
                A : { title : "总长时间通话时间（分钟）" },
                B : { title : "顶部持续时间长的国家的位置调用" } 
            }
        },
        inventory_report :{
            title : "清单"
        },
        billfeed_status :{
            title : "账务输送状态"
        },
        organization_hirechy : {
            title : "组织层次结构",
            chart : {
                A : { title : "组织层次结构树"},
                B : { title : "组织层次结构表"}
            }
        },
        exchange_rates : {
            title : "汇率",
            chart : {
                A: { title : "US$ to Local Currency C" }
            }
        },            
    },
    HELP_SCREEN : {
        title : "帮助",
        content : "<p>欢迎来到电信花经理应用程序！获取整个地区的移动通信消费和使用的概述。这个程序提出了相同的信息，您的TSM Web应用程序的仪表板。当在一个屏幕上，向左​​滑动或底部，以显示相关的度量其他图形。使用在屏幕的右上方定义来修改显示给你的愿望的数据范围的过滤器。.</p>"
    },
    SETTING_SCREEN : {
        title : "设置"
    },
    others : {
        select_country : "选择国家",
        select_period : "选择期",
        organized_by_country : "按国家排序",
        organized_by_charge_type : "按費用类型排序",
        select_charge_type : "选择费用类型",
        organized_by_month : "按月份排序",
        select_number_of_destination : "选择目的地数目",
        select_a_report_type : "选择报告类型",
        select_contributing_country : "选择贡献国",
        select_type_of_usage : "选择使用类型",
        select_no_of_users : "选择用户数",
        select_type_of_traffic : "选择通讯类型",
        select_number_of_top_long_duration_call : "选择长通话时间电话排名数目",
        select_destination : "选择目的地",
        local_currency_to_us$ : "当地货币为美元",
        us$_to_local_currency : "美元为当地货币",
        select_number_of_users : "选择用户数",
        expand : "扩大",
        collapse : "关闭"
    }
    
});