/**
 @classes singleton class which holds all translated static text in English
 Ext.ux.Localization determines language and than uses corresponding file from folder locales
 @example text: Loc.t('DASHBOARD_SCREEN.LABELS.SPEND')

    @package    Logixml
    @copyright  Copyright @ 2014
    @created    8 May 2014
    @author     Hardeep singh
 */

Ext.define('locales.ar', {
    extend: 'Ext.Base',

    singleton: true,
    
   CHART_SCREEN : {
        spend_month : {           
            title : "Spend Summary by Month",
            chart : {
                A : { title : "Total Spend" },
                B : { title : "Average Spend per Line" },
            }            
        },
        spend_country : {           
            title : "Spend Summary by Country",
            chart : {
                A : { title : "Total Spend"},
                B : { title : "Avg Spend per line Vs no. of Lines"}
            }     
        },
        spend_charge_type : {           
            title : "Spend Summary by Charge Type",
            chart : {
                A : { title : "Total Spend"},
                B : { title : "Average Spend per Line"}
            }     
        },
        spend_cost_center : {           
            title : "Spend Summary by Cost Center",
            chart : {
                A : { title : "Spend Cost Center Bar Chart"},
                B : { title : "Spend Cost Center Line Chart"}
            }     
        },
        spend_top_spender : {           
            title : "Spend Summary by Top Spender",
            chart : {
                A : { title : "Top Users Total Spend Over Selected Period"},
                B : { title : "Country Location of High Spending Users (by % of users)"}
            }     
        },
        spend_top_international : {           
            title : "Spend Summary by Top International",
            chart : {
                A : { title : "Top International Traffic Spend"},
                B : { title : "Top International Traffic Spend by %"}
            }     
        },
        spend_top_roaming : {           
            title : "Spend Summary by Top Roaming",
            chart : {
                A : { title : "Roaming Spend in Top Visited Countries"},
                B : { title : "Roaming Spend in Top Visited Countries by %"}
            }
        },
        
        usage_month : {
            title : "Usage Summary by Month",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line" }
                    }
                },
                B :{
                    title : "Data",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line" }
                    }
                },
                C :{
                    title : "Messaging",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line" }
                    }
                }
            }
        },
        usage_country : {
            title : "Usage Summary by Country",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line (Voice) Vs No. of Lines" }
                    }
                },
                B: {
                    title : "Data",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line (Data) Vs No. of Lines" }
                    }
                },
                C : {
                    title : "Messaging",
                    chart : {
                        A : { title : "Total Usage" },
                        B : { title : "Average Usage per Line (Messaging) Vs No. of Lines" }
                    }
                }              
            }
        },        
        usage_top_user : {
            title : "Usage Summary by Top User",
            chart : {
                A : { title : "Total Usage (of selected type) by Top High Volume Users" },
                B : { title : "Country Location of Top High Volume Users" }                              
            }
        },      
        usage_top_international : {
            title : "Usage Summary by Top International",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A : { title : "Top International Call Usage" },
                        B : { title : "Top Destinations for International Call Usage by %" }
                    }
                },
                B : {
                    title : "Messaging",
                    chart : {
                        A : { title : "Top International Messaging Usage" },
                        B : { title : "Top Destinations for International Messaging Usage by %" }
                    }
                }                         
            }
        },
        usage_top_roaming : {
            title : "Usage Summary by Top Roaming",
            chart : {
                A : {
                    title : "Calls",
                    chart : {
                        A: { title : "Top Roaming Calls (Minutes)" },
                        B: { title : "Top Destinations for Roaming Calls" }
                    }
                },
                B : {
                    title : "Messaging",
                    chart : {
                        A : { title : "Top Roaming Message (Nos)" },
                        B : { title : "Top Destinations for Roaming Message Usage" }
                    }
                },
                C : {
                    title : "Data",
                    chart : {
                        A : { title : "Top Roaming Data(MB)" },
                        B : { title : "Top Destinations for Roaming Data Usage" }
                    }
                }               
            }
        },
        usage_long_calls : {
            title : "Usage Summary by Long Calls",
            chart : {
                A : { title : "Total Long Duration Calls (Minutes)" },
                B : { title : "Country Location of Top Long Duration Calls" } 
            }
        },
        inventory_report :{
            title : "Inventory"
        },
        billfeed_status :{
            title : "Bilfeed Status"
        },
        organization_hirechy : {
            title : "Organization Hirechy",
            chart : {
                A : { title : "Organization Hirechy Tree"},
                B : { title : "Organization Hirechy Table"}
            }
        },
        exchange_rates : {
            title : "Exchange Rates",
            chart : {
                A: { title : "US$ to Local Currency" }
            }
        }
    }
    
});