/**
 * @class Ext.ux.localization.Config
 * @extend Ext.Base
 *
 * Singleton class that holds all localization configs
 * @author Bernhard Widtmann
 * @notes used in www.smart-order.com
 * @example
 *
 * var fallback = Ext.ux.localization.Config.fallback;
 *
*/

/**
 * By Hardeep Singh
 */
function get_locale()
{
    var arr = [];
    for (var i in lang_arr)
    {
        arr.push("locales." + lang_arr[i]);
    }
    
    return arr;
}

function get_support_lang()
{
    var arr = [];
    for (var i in lang_arr)
    {
        arr.push({
            lang : lang_arr[i],
            desc : i
        });
    }
    
    return arr;
}

Ext.define('Ext.ux.localization.Config', {
    extend: 'Ext.Base',
    requires: get_locale(),
    singleton: true,

    supported_languages: get_support_lang(),
    fallback: 'en', //language that should be used if locales file for current language is not available or translation is missing
    safe_text: 'translation missing' //text to be displayed if even in fallback language translation is missing

});