Ext.define('Ux.field.Multiselect', {
    extend: 'Ext.field.Select',
    alias : 'widget.multiselectfield',

    config: {
        delimiter: ',',

        mode: 'MULTI',

        doneButton: true,		
        CancelButton : true,
        SelectAllButton : true
    },
    /**
     * Updates the {@link #doneButton} configuration. Will change it into a button when appropriate, or just update the text if needed.
     * @param {Object} config
     * @return {Object}
     */
    applyDoneButton: function(config) {
		
        if (config) {
            if (Ext.isBoolean(config)) {
                config = {};
            }

            if (typeof config == "string") {
                config = {
                    text: config
                };
            }
		
            Ext.applyIf(config, {
                cls  : "button",
                style: {                                                                                                 
                    height  : "2.1875em",
                    width   : "2.1875em",
                    backgroundColor : "transparent",
                    backgroundImage : "url('img/check-mark.png')",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "100% 100%",
                    margin: '0.625em  0.5em',
                    border : "none"                            
                },
                listeners: {
                    tap: this.onButtonTap,
                    scope: this
                }
            });
			 
        }

        return Ext.factory(config, 'Ext.Button', this.getDoneButton());
    },
    applyCancelButton: function(config) {
		
        if (config) {
            if (Ext.isBoolean(config)) {
                config = {};
            }

            if (typeof config == "string") {
                config = {
                    text: config
                };
            }
		
            Ext.applyIf(config, {
                cls  : "button",
                style: {                                                                                                 
                    height  : "2.1875em",
                    width   : "2.1875em",
                    backgroundColor : "transparent",
                    backgroundImage : "url('img/cancel.png')",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "100% 100%",
                    margin: '0.625em  0.5em',
                    border : "none"                            
                },
                listeners: {
                    tap: this.onCancelTap,
                    scope: this
                }
            });
			 
        }

        return Ext.factory(config, 'Ext.Button', this.getCancelButton());
    },
    
    applySelectAllButton: function(config) {
		
        var me = this;
        if (config) {
            if (Ext.isBoolean(config)) {
                config = {};
            }

            if (typeof config == "string") {
                config = {
                    text: config
                };
            }
		
            Ext.applyIf(config, {
                cls  : "button",
                style: {                                                                                                 
                    height  : "2.1875em",
                    width   : "2.1875em",
                    backgroundColor : "transparent",
                    backgroundImage : "url('img/checkbox_checked.png')",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "100% 100%",
                    margin: '0.625em  0.5em',
                    border : "none"
                },
                listeners: {
                    tap: function (obj, e)
                    {                        
                        var style = obj.getStyle();
                        if (style.backgroundImage == "url('img/checkbox_checked.png')")
                        {
                            obj.setStyle({
                                backgroundImage : "url('img/checkbox_unchecked.png')"
                            });
                            me.onDeselectAll();
                        }
                        else
                        {
                            obj.setStyle({
                                backgroundImage : "url('img/checkbox_checked.png')"
                            });
                            me.onSelectAll();
                        }
                    },
                    scope: this
                }
            });
			 
        }

        return Ext.factory(config, 'Ext.Button', this.getSelectAllButton());
    },
    /**
     * @private
     */
    getTabletPicker: function() {
        var me = this,
            config = me.getDefaultTabletPickerConfig(),
            listMode = me.getMode(); 
        if (!me.listPanel) {
           
            me.listPanel = Ext.create('Ext.Panel', Ext.apply({
                modal: true,
				hidden: true,
				zIndex : "100",
                hideOnMaskTap: true,                 
                cls: Ext.baseCSSPrefix + 'select-overlay',
                layout: 'card',
                width: '24em',
                style : {
                  backgroundColor : report_color,
                },
                height: '26em',               
                items: [{
                    xtype: 'list',
                    itemCls : "list-label",
                    itemHeight : "3.125em",
                    mode : listMode,
                    store: me.getStore(),
                    itemMap: {
                        minimumHeight: "3.125em"
                    },
                    itemTpl: '<span class="x-list-label">{' + me.getDisplayField() + ':htmlEncode}</span>',
                    listeners : {
                        /*
                        erased : function () {
                            Ext.getCmp("filter_form").setMargin("0 1% 0 1%");
                        }, 
                            */
                            
                    }
                }],
                listeners : {
                    /*
                    show : function () {
                        var width = '22em';
                        this.setWidth(width);
                        Ext.getCmp("filter_form").setMargin("0 0 0 " + width);
                        
                    },
                    erased : function (me, opt) {
                        alert("hi");
                       Ext.Viewport.remove(me.listPanel,true);
                    }, 
                    */
                }
                
            }, config));

            if(listMode == 'SINGLE'){
                me.listPanel.down('list').on('itemtap',me.onListTap,me);
                me.listPanel.add({
                    xtype : "container",
                    layout: "hbox",
                    docked: 'top',
                    items : [
                        {
                            html : me._label,
                            style  : {
                                padding : "0.9375em 0 0.9375em 0.625em",
                                color  : "#fff",
                                fontSize : "1.1em"
                            }
                        }                        
                    ]
                });
            }else{
                me.listPanel.add({
                    xtype : "container",
                    layout : "hbox",
                    docked: 'top',
                    items : [                        
                        {
                            html : me._label,
                            style  : {
                                padding : "0.9375em 0 0.625em 0.625em",
                                color  : "#fff",
                                fontSize : "1.1em"
                            }
                        }, 
                        {
                            xtype : "spacer"
                        },
                        this.getSelectAllButton(),
                        this.getCancelButton(),
                        this.getDoneButton()
                    ]
                });
        }
      }
       // me.onSelectAll();
      
        return me.listPanel;
    },
    /**
     * @private
     */
    onListTap : function(list,index,target,record) 
    {
        this.setValue(record);
        this.callParent();
    },
    
    /**
     * @private
     */
    onButtonTap: function(){
        var records = this.listPanel.down('list').getSelection();
        this.setValue(records);
        this.superclass.onListTap.call(this);
    },
    onCancelTap : function (){
         this.superclass.onListTap.call(this);
    },
	onSelectAll: function(){		 
        var list = this.listPanel.down('list');        
        list.selectAll();
    },    
    onDeselectAll : function () {
        var list = this.listPanel.down('list');        
        list.deselectAll();
    },
	
    /**
     * @private
     */
    applyValue: function(value) {        
        this.getOptions();
        return  this.getValueFromRecords(value,this.getValueField());
    },
    /**
     * @private
     * this event fire when Select created and Close
     */
    updateValue: function(newValue, oldValue) {
        var me = this,
            value = me.convertValue(newValue,me.getValueField(),me.getDisplayField());
 
      value = value.join(me.getDelimiter());
      me.superclass.superclass.updateValue.call(me,[value]);
    },
    /**
     * @private
     */
    convertValue: function(value,fieldIn,fieldOut){
        var delimiter = this.getDelimiter(),
            store = this.getStore(),
            i = 0,
            out = [],
            len,
            item;
            
        if (value) {
            if (delimiter && Ext.isString(value)) {
                value = value.split(delimiter);
            } else if (!Ext.isArray(value)) {
                value = [value];
            }

            for (len = value.length; i < len; ++i) {
                item = store.findRecord(fieldIn,value[i]);
                if(item)
                    out.push(item.get(fieldOut));
            }
        }
        return out;
    },
    /**
     * @private
     * Returns the value in array form from records
     */
    getValueFromRecords: function(value){
        var delimiter = this.getDelimiter(),
            valueField = this.getValueField(),
            i = 0,
            out = [],
            len,
            item;
            
        if (value) {
            if (delimiter && Ext.isString(value)) {
                value = value.split(delimiter);
            } else if (!Ext.isArray(value)) {
                value = [value];
            }
        
            for (len = value.length; i < len; ++i) {
                item = value[i];
                if (item && item.isModel) {
                    out.push(item.get(valueField));
                }
            }
            out = Ext.Array.unique(out);
        }
        return out.length > 0 ? out : value;
    },
    /**
     * @private
     */
    getRecordsFromValue: function(value){
        var records = [],
            all = this.getStore().getRange(),
            valueField = this.getValueField(),
            i = 0,
            allLen = all.length,
            rec,
            j,
            valueLen;

        if(value){
            for (valueLen = value.length; i < valueLen; ++i) {
                for (j = 0; j < allLen; ++j) {
                    rec = all[j];   
                    if (rec.get(valueField) == value[i]) {
                        records.push(rec);
                        break;
                    }
                }   
            }
        }
        return records;
    },
    /**
     * Returns the current selected {@link Ext.data.Model records} instances selected in this field.
     * @return {Ext.data.Model[]} An array of Records.
     */
    getSelection: function() {
        return this.listPanel.down('list').getSelection();
    },
    /**
     * Returns the current selected records as an array of their valueFields.
     * @return {Array} An array of valueFields
     */
    getValue: function() {
        return this._value;
    },
    /**
     * @private
     */
    onChange: function(component, newValue, oldValue) {
        var me = this,
            old = me.convertValue(oldValue,me.getDisplayField(),me.getValueField());
        
        me.fireEvent('change', me, me.getValue(), old);
    },
    /**
     * Shows the picker for the select field, whether that is a {@link Ext.picker.Picker} or a simple
     * {@link Ext.List list}.
     */
    showPicker: function() {
        var me = this,
            store = this.getStore();

        //check if the store is empty, if it is, return
        if (!store || store.getCount() === 0) {
            return;
        }

        if (me.getReadOnly()) {
            return;
        }

        me.isFocused = true;

        var listPanel = me.getTabletPicker(),
            list = listPanel.down('list'),
            index, records,
            value = me.getValue();

        if (!listPanel.getParent()) {
            Ext.Viewport.add(listPanel);
        }

        if(value){
            records = me.getRecordsFromValue(value);
            list.select(records, null, true);
        }else{
            list.deselectAll();
        }

        listPanel.showBy(me.getComponent(), (Ext.os.is.BlackBerry && Ext.os.version.getMajor() === 10) ? 't-b' : null);
    },
    /**
     * Called when the internal {@link #store}'s data has changed.
     */
    onStoreDataChanged: function(store) {
        var me = this,
            initialConfig = me.getInitialConfig(),
            value = me.getValue();

        if (value || value === 0) {
            me.updateValue(me.applyValue(value));
        }

        if (me.getValue() === null) {
            if (initialConfig.hasOwnProperty('value')) {
                me.setValue(initialConfig.value);
            }

            if (me.getValue() === null && me.getAutoSelect()) {
                if (store.getCount() > 0) {
                    me.setValue(store.getAt(0));
                }
            }
        }
    },
});
